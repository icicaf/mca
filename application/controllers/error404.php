<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Error404 extends CI_Controller 
{ 
   public function index()
   {
      echo $this->error_404();
   }

   public function error_404()
   {
       return $this->load->view('errores/Verror-404',NULL,TRUE);
   }
}