<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clogin extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		$this->load->database();
    }

	/**
	 * Vista para Login
	 *
	*/
	public function index()
	{
        $this->load->view('login/Vlogin');
    }

    /**
	 * 
	 *
	*/
	public function validar_usuario()
	{
		//ECHO 'iniciando validacion de usuario';
		//ECHO '<BR>';
		$data["usuario"] = $this->input->post("usuario");
		$data["clave"] = $this->input->post("clave");

		$this->load->model('Musuarios');

		$data['credenciales'] = $this->Musuarios->get_usuario_login($data["usuario"],$data["clave"]);
		//print_r($data['credenciales']);
		//ECHO '<BR>';

		if($data['credenciales'] != NULL)
		{
			$this->session->set_userdata(array('usuario_id' => $data['credenciales'][0]['usuario_id'],
				'username' => $data['credenciales'][0]['usuario_username'],
				'nombre' => $data['credenciales'][0]['usuario_nombre'],'logged_in' => TRUE));
			
			redirect(base_url().'Cmca');
		}
		else 
		{
			$this->session->set_flashdata('login', '1');
			redirect(base_url());
		}
	}
	
	public function salir()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
}