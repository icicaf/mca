<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cmca extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		
		if($this->session->userdata('logged_in') != TRUE)
		{
			redirect(base_url());
		}		
    }
	/**
	 * Modulos principales sistemas
	 *
	*/
	public function index()
	{
		//Modulos Vistas
		$data["html_head"] = $this->get_htmlHead();
		$data["html_topbar"] = $this->get_htmlTopbar();
		$data["html_topmenu"] = $this->get_htmlTopmenu();
		$data["html_breadcrumb"] = $this->get_htmlBreadcrumb(array('Inicio',''));
		$data["html_bodycentral"] = $this->get_htmlBodycentral();
		$data["html_footer"] = $this->get_htmlFooter();

		//Scipt MCA
		$data["html_scriptmca"] = $this->get_htmlSciptMca();

		//Modulos Script
		$data["script_inicio"] = $this->modulo_inicio();
		$data["script_ventas"] = $this->modulo_ventas();
		$data["script_productos"] = $this->modulo_productos();
		$data["script_cuenta"] = $this->modulo_cuenta();
		$data["script_usuarios"] = $this->modulo_usuarios();

		
		//Modulo Carga Vistas
		$this->load->view('mca/Vmca',$data);
	}

	public function modulo_inicio()
	{
		return $this->load->view('inicio/Vscript_inicio_menu',NULL,TRUE);	
	}

	public function modulo_ventas()
	{
		$data["html_breadcrumb"] = $this->get_htmlBreadcrumb(array('Ventas',''));
		
		return $this->load->view('ventas/Vscript_ventas_menu',NULL,TRUE);
	}

	public function modulo_productos()
	{
		return $this->load->view('productos/Vscript_productos_menu',NULL,TRUE);
	}

	public function modulo_usuarios()
	{	
		return $this->load->view('usuarios/Vscript_usuarios',NULL,TRUE);
	}

	public function modulo_cuenta()
	{
		return $this->load->view('cuenta/Vscript_cuenta',NULL,TRUE);
	}

	public function modulo_inventarios()
	{
		
	}

	public function modulo_reportes()
	{
		
	}

	public function modulo_estadisticas()
	{
		
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Componentes principal HEAD html
	 *
	*/
	public function get_htmlHead()
	{
		return $this->load->view('head/Vhead',NULL,TRUE);
	}

	/**
	 * Componentes principal TOPBAR html
	 *
	*/
	public function get_htmlTopbar()
	{
		return $this->load->view('topbar/Vtopbar',NULL,TRUE);
	}

	/**
	 * Componentes principal TOPBAR html
	 *
	*/
	public function get_htmlTopmenu()
	{
		return $this->load->view('topmenu/Vtopmenu',NULL,TRUE);
	}

	/**
	 * Componentes principal BREADCRUMB html - Array{'titulo_menu','titulo_opcion'}
	 *
	*/
	public function get_htmlBreadcrumb($breadcrumb=NULL)
	{
		$data['breadcrumb'] = $breadcrumb;
		//print_r($data['breadcrumb']);
		return $this->load->view('breadcrumb/Vbreadcrumb',$data,TRUE);
	}

	/**
	 * Componentes principal BODYCENTRAL html
	 *
	*/
	public function get_htmlBodycentral()
	{
		return $this->load->view('bodycentral/Vbodycentral',NULL,TRUE);
	}

	/**
	 * Componentes principal FOOTER html
	 *
	*/
	public function get_htmlFooter()
	{
		return $this->load->view('footer/Vfooter',NULL,TRUE);
	}

	/**
	 * Componentes principal FOOTER html
	 *
	*/
	public function get_htmlSciptMca()
	{
		return $this->load->view('script/Vscriptmca',NULL,TRUE);
	}
}