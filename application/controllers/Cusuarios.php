<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cusuarios extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		$this->load->database();
    }

	/**
	 * Vista
	 *
	*/
	public function index()
	{
        ECHO 'CONTROLADOR OK';
    }

    public function gestion_usuario()
    {
        $this->load->view('usuarios/Vusuarios');
    }

    //----------------------------------------------------------------------------------------
    public function formulario_ingreso_nuevo_usuario()
	{
		$this->load->model('musuarios');
		$data["tipos_usuarios"] = $this->musuarios->get_tipos_usuarios();
		$data["detalle_usuario"] = NULL;
		$data["realizar_accion"] = "Registrar Nuevo Usuario";
		$data["descripcion_accion"] = "Completar el formulario a continuación para el correcto ingreso del nuevo usuario.";

		$this->load->view('usuarios/Vingresar_nuevo_usuario',$data);
	}

	public function ingresar_nuevo_usuario()
	{
		$this->load->model('musuarios');
		
		$user_nombre = $this->input->post('user_nombre');
		$user_apellidos = $this->input->post('user_apellidos');
        $user_rut = $this->input->post('user_rut');
        $user_correo = $this->input->post('user_correo');
        $select_tipo_usuario = $this->input->post('select_tipo_usuario');
        $id_usuario = $this->input->post('id_user');

		// print_r("user_nombre: ".$user_nombre."\n");
		// print_r("user_apellidos: ".$user_apellidos."\n");
		// print_r("user_rut: ".$user_rut."\n");
		// print_r("user_correo: ".$user_correo."\n");
		// print_r("select_tipo_usuario: ".$select_tipo_usuario."\n");

		$primera_letra = substr($user_nombre,0,1);
		$aux = explode(' ', $user_rut);
		$primer_apellido = $aux[0];

		$aux2 = explode('-', $user_rut);
		$usuario_username = $aux2[0];

		//$usuario_username = $primera_letra.$primer_apellido;
		$usuario_password = $primera_letra.$usuario_username;
		$usuario_nombre = $user_nombre." ".$user_apellidos;
		$usuario_fecha_creacion = date('Y-m-j H:i:s');

		$data_usuario = array(	'usuario_id' => $id_usuario,
								'usuario_username' => $usuario_username,
								'usuario_password' => $usuario_password,
								'usuario_nombre' => $usuario_nombre,
								'usuario_rut' => $user_rut,
								'usuario_correo' => $user_correo,
								'usuario_fecha_creacion' => $usuario_fecha_creacion,
								'tipo_usuario_id' => $select_tipo_usuario);

		$data = $this->musuarios->ingresar_datos_usuario($data_usuario);
		print_r($data);
	}

	public function actualizar_usuario()
	{
		$this->load->model('musuarios');
		
		$user_nombre = $this->input->post('user_nombre');
		$user_apellidos = $this->input->post('user_apellidos');
		$user_password = $this->input->post('user_password');
        $user_correo = $this->input->post('user_correo');
        $select_tipo_usuario = $this->input->post('select_tipo_usuario');
        $id_usuario = $this->input->post('id_user');

		$usuario_nombre = $user_nombre." ".$user_apellidos;
		$usuario_fecha_modificacion = date('Y-m-j H:i:s');

		$data_usuario = array(	'usuario_id' => $id_usuario,
								'usuario_password' => $user_password,
								'usuario_nombre' => $usuario_nombre,
								'usuario_correo' => $user_correo,
								'usuario_fecha_modificacion' => $usuario_fecha_modificacion,
								'tipo_usuario_id' => $select_tipo_usuario);

		$data = $this->musuarios->ingresar_datos_usuario($data_usuario);
		print_r($data);
	}

	public function gestion_usuarios_registrados()
	{
		$this->load->model('musuarios');
		$data["usuarios"] = $this->musuarios->get_usuarios();

		$this->load->view('usuarios/Vtabla_usuarios',$data);
	}

	public function get_detalle_usuario()
	{
		$this->load->model('musuarios');
		$id_usuario = $this->input->post('id_usuario');
		//print_r($tipos_usuarios);

		$data["tipos_usuarios"] = $this->musuarios->get_tipos_usuarios();
		$data["detalle_usuario"] = $this->musuarios->get_detalle_usuario($id_usuario);
		$data["realizar_accion"] = "Modificar Usuario Existente";
		$data["descripcion_accion"] = "Completar el formulario a continuación para la correcta modificacion del usuario.";

		$this->load->view('usuarios/Vingresar_nuevo_usuario',$data);
	}

	public function eliminar_usuario()
	{
		$this->load->model('musuarios');
		$id_usuario = $this->input->post('id_usuario');
		$usuario_fecha_modificacion = date('Y-m-j H:i:s');

		$data_usuario = array('usuario_id' => $id_usuario,
							  'usuario_habilitado' => 0,
							  'usuario_fecha_modificacion' => $usuario_fecha_modificacion);

		$data = $this->musuarios->eliminar_usuario($data_usuario);
		echo $data;
	}
	//----------------------------------------------------------------------------------------
}