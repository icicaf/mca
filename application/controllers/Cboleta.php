<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// - Factura Electrónica
// - Factura No Afecta o Exenta Electrónica
// - Guía de Despacho Electrónica
// - Nota de Crédito Electrónica
// - Nota de Débito Electrónica
// - Factura de Compra Electrónica
// - Liquidación Factura Electrónica
// - Factura de Exportación Electrónica
// - Nota de Crédito de Exportación Electrónica
// - Nota de Débito de Exportación Electrónica

class Cboleta extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		$this->load->database();
    }

    public function get_formato_boleta()
    {
        $this->load->library('Pdf');
        $this->load->view('boleta/Vformato_boleta.php');
    }

    public function imprimir_comprobante_venta()
    {
        $salto_linea = 20;
        $linea = 0;

        $handle = printer_open('post_impresora');
        printer_start_doc($handle, "boleta");
        printer_start_page($handle);

        $font = printer_create_font("Arial", 17, 8, PRINTER_FW_NORMAL, false, false, false, 0);
        printer_select_font($handle, $font);
        printer_draw_text($handle, "COMPROBANTE DE VENTA", 80, $linea);
        $linea = $linea + $salto_linea;
        printer_draw_text($handle, "RUT: 16.675.738-K", 100, $linea);
        $linea = $linea + $salto_linea;
        printer_draw_text($handle, "NOMBRE DEL GIRO O NOMBRE DEL NEGOCIO", 0, $linea);
        $linea = $linea + $salto_linea;
        printer_draw_text($handle, utf8_encode("DIRECCION DEL LOCAL"), 50, $linea);
        $linea = $linea + $salto_linea;
        $linea = $linea + $salto_linea;
        printer_draw_text($handle, 'FECHA', 0, $linea);
        printer_draw_text($handle, 'HORA', 180, $linea);
        printer_draw_text($handle, 'TERMINAL', 320, $linea);
        $linea = $linea + $salto_linea;
        printer_draw_text($handle, date('d-m-Y'), 0, $linea);
        printer_draw_text($handle, date('H:i'), 180, $linea);
        printer_draw_text($handle, 'CAJA 1', 320, $linea);

        $linea = $linea + $salto_linea;
        printer_delete_font($font);


        $fontprd = printer_create_font("Arial", 17, 8, PRINTER_FW_NORMAL, false, false, false, 0);
        printer_select_font($handle, $fontprd);
        printer_draw_text($handle, "RUT: 16.675.738-K", 60, $linea);
        $linea = $linea + $salto_linea;


        $linea =( $linea + $salto_linea)*2;
        printer_draw_text($handle, "GRACIAS POR SU COMPRA", 60, $linea);
        printer_delete_font($font);

        // echo $linea;
        // $brush = printer_create_brush(PRINTER_BRUSH_SOLID, "2222FF");
        // printer_select_brush($handle, $brush);
        // printer_draw_rectangle($handle, $linea, 10, 0, $linea);
        // printer_delete_brush($brush);
        // printer_delete_pen($pen);

        printer_end_page($handle);
        printer_end_doc($handle);
        printer_close($handle);
    }

    public function genera_dte($id_venta)
    {
        $this->load->model('mventas');
        $this->load->library("Pdf");

        $data["data_venta"] = $this->mventas->get_venta($id_venta);
        $data_dte = $this->get_dte(0,$data);

       // header("Content-type: application/pdf");
        
        $pdf = new Pdf('P', 'mm', 'boleta', FALSE, 'UTF-8', false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        //$pdf->SetMargins(5, 1, 0);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
 
        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 
        //relación utilizada para ajustar la conversión de los píxeles
        $pdf->SetMargins(1,1,0,0);
        //$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->SetFont('dejavusans', '', 8, '', true);
        $pdf->AddPage('P','boleta');


        $html = $data_dte;
        $pdf->writeHTML($html, true, false, true, false, '');

        // force print dialog
        $js = 'print(true);';

        $pdf->IncludeJS($js);

        $nombre_archivo = utf8_decode("comprobante_venta_".$id_venta.".pdf");
        $pdf->Output($nombre_archivo, 'I');
    }

    public function get_dte($tipo_dte,$data)
    {
        // 0: comprobante de venta
        // 33: Factura Electrónica
        // 34: Factura No Afecta o Exenta Electrónica
        // 43: Liquidación-Factura Electrónica
        // 46: Factura de Compra Electrónica
        // 52: Guía de Despacho Electrónica
        // 56: Nota de Débito Electrónica
        // 61: Nota de Crédito Electrónica
        // 110: Factura de Exportación
        // 111: Nota de Débito de Exportación
        // 112: Nota de Crédito de Exportación
        
        $dte = '';

        switch ($tipo_dte) 
        {
            case 0:
                $dte = $this->load->view('dte/Vformato_boleta',$data,TRUE);
            break;
        }

        return $dte;
    }
}