<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cinicio extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
		ECHO 'CONTROLADOR OK';
	}

	public function vista_resumen_sistema()
	{
		$this->load->model('minicio');
		$hoy = date("Y-m-d");
		$fecha_hoy = date("d-m-Y");

		$data['hoy'] = $fecha_hoy;
		$data['venta_diaria'] = $this->minicio->get_venta_diaria();
		$data['venta_semanal'] = $this->minicio->get_venta_semanal();
		$data['venta_mensual'] = $this->minicio->get_venta_mensual();

		$this->load->view('inicio/Vinicio',$data);
	}
}