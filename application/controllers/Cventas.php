<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cventas extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
    }

	/**
	 * Index Page for this controller.
	 *
	 */

	public function index()
	{
		ECHO 'CONTROLADOR VENTAS OK';
	}

	public function ingresar_producto()
	{
		$this->load->view('ventas/Vventas');
	}

	public function getDatosPorductos()
	{
		$this->load->model('Mventas');
		$cb = $this->input->post('CBarra');		
		$data= $this->Mventas->getDatosProducto($cb);

		if (empty($data)) 
		{
			echo '{"producto_id":"0","producto_codigo":"0","producto_nombre":"0","producto_descripcion":"0","producto_valor_compra":"0","producto_valor_venta":"0","producto_stock_actual":"0","producto_stock_minimo":"0","producto_fecha_ingreso":"","tipo_producto_id":"0","usuario_id":"0"}';
		}
		else
		{
			 echo  json_encode($data[0]);
		}
	}

	public function realizarPago()
	{
		$this->load->model('Mventas');

		$data_productos = $this->input->post('productos');	
		$data_medio_pago = $this->input->post('medio');
		$data_total_venta = $this->input->post('total');
		$data_usuario = $this->session->userdata('usuario_id');

		$xml = '<?xml version="1.0" encoding="utf-8"?><boleta></boleta>';
		$boleta_xml = simplexml_load_string($xml);

		foreach ($data_productos as $key) 
		{
			$item = $boleta_xml->addChild('item');
			$item->addChild('cantidad_producto', $key[0]);
			$item->addChild('codigo_producto', $key[1]);
			$item->addChild('nombre_producto', $key[2]);
			$item->addChild('precio_producto', $key[3]);
		}

		$total_venta = $boleta_xml->addChild('total');
		$total_venta->addChild('total_venta', $data_total_venta);

		$xml = $boleta_xml->asXML();

		//EJECUTO LA TRANSACCION DE VENTA
		$data_id_venta = $this->Mventas->transact_venta($xml,
												$data_productos,
												$data_medio_pago,
												$data_total_venta,
												$data_usuario);
		//consulta si la venta fue realizada
		// id > 0 venta realizada
		// id = 0 venta no se realizo o se presento un problema en la transaccion

		if($data_id_venta != 0)
		{
			echo $data_id_venta;
		}
		else
		{
			echo 0;
		}
	}
}