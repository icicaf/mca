<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cproductos extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
		ECHO 'CONTROLADOR OK';
	}

	public function vista_escanear_producto()
	{
		$this->load->view('productos/Vsolicita_escanear_codigo');
	}

	public function vista_formulario_producto()
	{
		$this->load->model('mproductos');
		
		$data["product_code"] = $this->input->post('product_code');
		$data["existencia"] = $this->input->post('existencia');
		$data["datos_producto"] = $this->mproductos->get_datos_producto($data["product_code"]);

		$product_ID = $this->mproductos->get_id_producto($data["product_code"]);
		if($product_ID!=null){
			$data["product_ID"] = $product_ID[0]["producto_id"];
		}else{
			$data["product_ID"] = "";
		}

		$data['tipos_productos'] = $this->mproductos->get_tipos_productos();
		$this->load->view('productos/Vregistrar_productos',$data);
	}

	public function seleccion_listado_productos()
	{
		$this->load->model('mproductos');
		$data['tipos_productos'] = $this->mproductos->get_tipos_productos();
		$this->load->view('productos/Vseleccion_listado_productos',$data);
	}

	public function revisar_inventario()
	{
		$this->load->model('mproductos');
		$data['cantidad_productos_tipo'] = $this->mproductos->get_cantidad_productos_tipo();
		$this->load->view('productos/Vrevisar_inventario',$data);
	}

	//----------------------------------------------------------------------------------------
	public function verifica_existencia_codigo()
	{
		$this->load->model('mproductos');
		$product_code = $this->input->post('product_code');
		$data = $this->mproductos->get_existencia_producto($product_code);
		//print_r($data);
		if($data[0]["producto_habilitado"]==null){
			echo 0;
		}else if($data[0]["producto_habilitado"]==1){
			echo 1;
		}else if($data[0]["producto_habilitado"]==0){
			echo 2;
		}
	}

	public function get_datos_producto()
	{
		$this->load->model('mproductos');
		$product_code = $this->input->post('product_code');
		$data = $this->mproductos->get_datos_producto($product_code);
		print_r($data);
	}


	public function ingresar_producto()
	{
		$this->load->model('mproductos');
		
		$id_pro = $this->input->post('id_producto');
		$codigo = $this->input->post('codigo');
        $nombre = $this->input->post('nombre');
        $descri = $this->input->post('descri');
        $docume = $this->input->post('documento');
        $compra = $this->input->post('compra');
        $venta  = $this->input->post('venta');
        $stock_ingreso = $this->input->post('stock_ingreso');
        $stock_minimo  = $this->input->post('stock_minimo');
        $stock_actual  = $this->input->post('stock_actual');
        $tipo_producto = $this->input->post('tipo_producto');

		// print_r("id_pro: ".$id_pro."\n");
		// print_r("codigo: ".$codigo."\n");
		// print_r("nombre: ".$nombre."\n");
		// print_r("descri: ".$descri."\n");
		// print_r("docume: ".$docume."\n");
		// print_r("compra: ".$compra."\n");
		// print_r("venta: ".$venta."\n");
		// print_r("stock_ingreso: ".$stock_ingreso."\n");
		// print_r("stock_minimo: ".$stock_minimo."\n");
		// print_r("stock_actual: ".$stock_actual."\n");
		// print_r("tipo_producto: ".$tipo_producto."\n");

		$fecha_ingreso = date('j-m-Y');
		$hora_ingreso = date('h:i:s');

		$data_producto = array(		'producto_id' => $id_pro,
									'producto_codigo' => $codigo,
									'producto_nombre' => $nombre,
									'producto_descripcion' => $descri,
									'producto_valor_compra' => $compra,
									'producto_valor_venta' => $venta,
									'producto_stock_actual' => ($stock_ingreso+$stock_actual),
									'producto_stock_minimo' => $stock_minimo,
									'producto_fecha_ingreso' => $fecha_ingreso,
									'tipo_producto_id' => $tipo_producto,
									'usuario_id' => '16155894',
									'producto_habilitado' => 1);

		$data_producto_entrada = array(	'producto_id' => $id_pro,
										'producto_entrada_cantidad' => $stock_ingreso,
										'producto_entrada_valor_compra' => $compra,
										'producto_entrada_valor_venta' => $venta,
										'producto_entrada_numero_documento' => $docume,
										'producto_entrada_fecha' => $fecha_ingreso,
										'producto_entrada_hora' => $hora_ingreso,
										'usuario_id' => '16155894');

		$data = $this->mproductos->ingresar_producto($data_producto,$data_producto_entrada);
		return $data;
	}

	public function deshabilitar_producto()
	{
		$this->load->model('mproductos');

		$id_pro = $this->input->post('idProducto');
		$data = $this->mproductos->deshabilitar_producto($id_pro);
		return $data;
	}

	//----------------------------------------------------------------------------------------
	public function get_listado_productos_segun_tipo()
	{
		$this->load->model('mproductos');
		$tipo_producto_id = $this->input->post('tipo');
		$datos["tabla_productos"] = $this->mproductos->get_listado_productos_segun_tipo($tipo_producto_id);
		$this->load->view("productos/Vtabla_productos",$datos);
	}

	public function get_cantidad_productos_tipo()
	{
		$this->load->model('mproductos');
		$data = $this->mproductos->get_cantidad_productos_tipo();
		return $data;
	}

	public function get_detalle_tipo_productos()
	{

		$this->load->model('mproductos');
		$producto_id = $this->input->post('id_tipo_producto');
		$detalle_tipo_productos = $this->mproductos->get_detalle_tipo_productos($producto_id);
		//print_r($data);

		$html = '';
		$html.= 
			'<table class="table table-striped table-bordered table-hover">';
			foreach ($detalle_tipo_productos as $key){
				$html.=  
					'<tbody>
						<tr>
							<td class="info" style="text-align:center;"><b>TIPO DE PRODUCTOS</b></td>
							<td>
								<input rows="4" cols="40" value="'.$key['tipo_producto_nombre'].'" style="text-transform:uppercase" disabled></input>
							</td>
						</tr>

						<tr>
							<td class="info" style="text-align:center;"><b>Cantidad de Productos</b></td>
							<td>
								<input rows="4" cols="40" value="'.$key['cant_productos'].'"disabled></input>
							</td>
						</tr>
						
						<tr>
							<td class="info" style="text-align:center;"><b>Bajo del Stock Mínimo</b></td>
							<td>
								<input rows="4" cols="40" value="'.$key['productos_bajo_stock_minimo'].'" disabled></input>
							</td>
						</tr>
						<tr>
							<td class="info" style="text-align:center;"><b>Productos Sin Stock</b></td>
							<td>
								<input rows="4" cols="40" value="'.$key['productos_sin_stock'].'" disabled></input>
							</td>
						</tr>
						<tr>
							<td class="info" style="text-align:center;"><b>Productos Deshabilitados</b></td>
							<td>
								<input rows="4" cols="40" value="'.$key['productos_deshabilitados'].'" disabled></input>
							</td>
						</tr>
						<tr>
							<td class="info" style="text-align:center;"><b>Productos Sin Precio</b></td>
							<td>
								<input rows="4" cols="40" value="'.$key['productos_sin_precio'].'" disabled></input>
							</td>
						</tr>
					</tbody>';
			}
			$html.= '</table>';
		echo $html;
	}
}