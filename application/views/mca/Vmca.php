<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
    <!-- Head -->
    <?php echo $html_head; ?>
    <!-- Fin Head -->
    
    <body id="id_body">
        <!-- Navigation Bar-->
        <header id="topnav">
            <?php ECHO $html_topbar; ?>
            <?php ECHO $html_topmenu; ?>
            <?php ECHO $script_inicio; ?>
            <?php ECHO $script_ventas; ?>
            <?php ECHO $script_productos; ?>
            <?php ECHO $script_cuenta; ?>
            <?php ECHO $script_usuarios; ?>
        </header>
        <!-- End Navigation Bar-->

        <div class="wrapper">
            <div class="container-fluid">
                <!-- Page-Title -->
                <?php ECHO $html_breadcrumb; ?>                
                <!-- end page title end breadcrumb -->
                <?php ECHO $html_bodycentral; ?>
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->
        
        <!-- Footer -->
        <?php echo $html_footer; ?>
        <!-- End Footer -->
        <!-- Script MCA -->
    <?php echo $html_scriptmca; ?>
    <!-- End Script MCA -->
    </body>
</html>