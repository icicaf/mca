<div id="modalDetallePago" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mymodalDetallePago" aria-hidden="true" data-keyboard="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button style="cursor: pointer;" onclick="cerrarPago();" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="mymodalDetallePago">Detalle de Pago</h4>
            </div>            
            <div class="modal-body">
                <div id="contenido_modal_detalle"></div>
                <hr>
                <h4>Medio de pago</h4>
                <div>
                    <div class="radio radio-success">
                        <input type="radio" name="radio" id="radio1" value="option1" checked="checked">
                        <label for="radio1">Efectivo</label>
                    </div>
                    <div class="radio radio-success">
                        <input type="radio" name="radio" id="radio2" value="option2">
                        <label for="radio2">Credito</label>
                    </div>
                    <div class="radio radio-success">
                        <input type="radio" name="radio" id="radio3" value="option3">
                        <label for="radio3">Debito</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button onclick="enviarPago();" type="button" class="btn btn-primary waves-effect waves-light">Pagar</button>
                <button onclick="cerrarPago();" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    #listtableWrapperScroll{
        height:400px; 
    overflow-y:scroll;
    
    }
    #listtableWrapperScroll #tablacss{
        height:100px;     
    }

    #listtableWrapperScroll #tablacss tr td{
        border-bottom:1px dashed #444;
        height:100px;
    }
</style>

<div class="card-box">
    <div class="container">
        <div class="row">
            <!-- <div class="col-sm">
                <h2 >Registrar venta</h2>            
            </div> -->
            <div class="col-sm-4">
                <div class="input-group">
                    <span onclick="ingresarProductos();" class="input-group-addon btn-success waves-effect waves-light"><b>Agregar &nbsp;</b> <i class="mdi mdi-barcode-scan"></i></span>
                    <input class="scanner form-control" class="form-control" id="entrada" value="" autofocus onblur>
                    &nbsp;
                    <!-- <button onclick="ingresarProductos();" class="btn btn-success waves-effect waves-light">Agregar</button> -->
                </div>
                <p class="text-muted font-14 m-b-20">Registre los codigos de productos para realizar la venta</p>
            </div>
            <div class="col-sm">
                <h3><div id="clock2" style='text-align:right'></div></div></h3>
            </div>
        </div>
        <div class="col-sm-12">
            <table class="table table-bordered"  id="productos">
                <thead class="table-info">
                    <tr>
                        <th width="30px"><b>Cantidad</b></th>
                        <th width="300px"><b>Producto</b></th>
                        <th width="200px"><b>Valor </b></th>
                        <th><b>Codigo</b></th>
                        <th  width="30px"><b>Agregar</b></th>
                        <th  width="30px"><b>Quitar</b></th>
                        <th  width="30px"><b>Eliminar</b></th>
                    </tr>
                </thead>
            </table>
        <div>
        <div class="col-sm-12" id="listtableWrapperScroll">
            <table class="table table-bordered" id="tablacss">
                <tbody id="Tventas"></tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-sm-12" align="center">
                <h2 id="total">Total: $0</h2>
                <button type="button" id="btn_pagar" onclick="ingresarPago();" class="btn btn-primary waves-effect waves-light w-md m-b-25">Pagar</button>
            </div>  
        <div> 
</div>

<script>
    (function(c,v)
    {
        c.clock={version:"2.0.2",
        locale:{}};
        t=[];

        c.fn.clock=function(a)
        {
            var e={it:{weekdays:"Domenica Luned\u00ec Marted\u00ec Mercoled\u00ec Gioved\u00ec Venerd\u00ec Sabato".split(" "),
            months:"Gennaio Febbraio Marzo Aprile Maggio Giugno Luglio Agosto Settembre Ottobre Novembre Dicembre".split(" ")},
            en:{weekdays:"Domingo Lunes Martes Miercoles Jueves Viernes Sabado".split(" "),
            months:"Enero Febrero Marzo Abril Mayo Junio Julio Agosto Septiembre Octubre Noviembre Diciembre".split(" ")},
            es:{weekdays:"Domingo Lunes Martes Mi\u00e9rcoles Jueves Viernes S\u00e1bado".split(" "),
            months:"Enero Febrero Marzo Abril May junio Julio Agosto Septiembre Octubre Noviembre Diciembre".split(" ")},
            de:{weekdays:"Sonntag Montag Dienstag Mittwoch Donnerstag Freitag Samstag".split(" "),
            months:"Januar Februar M\u00e4rz April k\u00f6nnte Juni Juli August September Oktober November Dezember".split(" ")},
            fr:{weekdays:"Dimanche Lundi Mardi Mercredi Jeudi Vendredi Samedi".split(" "),
            months:"Janvier F\u00e9vrier Mars Avril May Juin Juillet Ao\u00fbt Septembre Octobre Novembre D\u00e9cembre".split(" ")},
            ru:{weekdays:"\u0412\u043e\u0441\u043a\u0440\u0435\u0441\u0435\u043d\u044c\u0435 \u041f\u043e\u043d\u0435\u0434\u0435\u043b\u044c\u043d\u0438\u043a \u0412\u0442\u043e\u0440\u043d\u0438\u043a \u0421\u0440\u0435\u0434\u0430 \u0427\u0435\u0442\u0432\u0435\u0440\u0433 \u041f\u044f\u0442\u043d\u0438\u0446\u0430 \u0421\u0443\u0431\u0431\u043e\u0442\u0430".split(" "),months:"\u042f\u043d\u0432\u0430\u0440\u044c \u0424\u0435\u0432\u0440\u0430\u043b\u044c \u041c\u0430\u0440\u0442 \u0410\u043f\u0440\u0435\u043b\u044c \u041c\u0430\u0439 \u0418\u044e\u043d\u044c \u0418\u044e\u043b\u044c \u0410\u0432\u0433\u0443\u0441\u0442 \u0421\u0435\u043d\u0442\u044f\u0431\u0440\u044c \u041e\u043a\u0442\u044f\u0431\u0440\u044c \u041d\u043e\u044f\u0431\u0440\u044c \u0414\u0435\u043a\u0430\u0431\u0440\u044c".split(" ")},
            nu:{months:[1,2,3,4,5,6,7,8,9,10,11,12]}
        };
        
        return this.each(function()
        {
            c.extend(e,c.clock.locale);
            a=a||{};
            a.timestamp=a.timestamp||"systime";systimestamp=new Date;systimestamp=systimestamp.getTime();a.sysdiff=0;"systime"!=a.timestamp&&(mytimestamp=new Date(a.timestamp),a.sysdiff=a.timestamp-systimestamp);a.langSet=a.langSet||"en";a.format=a.format||("en"!=a.langSet?"24":"12");
            a.calendar=a.calendar||"true";a.seconds=a.seconds||"true";
            c(this).hasClass("jqclock")||c(this).addClass("jqclock");
            var l=function(a)
            {
                10>a&&(a="0"+a);
                return a
            },
            u=function(k,b)
            {
                var f,s=c(k).attr("id");

                if("destroy"==b)
                {
                    clearTimeout(t[s]);
                }
                else
                {
                    mytimestamp=new Date;
                    mytimestamp=mytimestamp.getTime();
                    mytimestamp+=b.sysdiff;
                    mytimestamp=new Date(mytimestamp);
                    var d=mytimestamp.getHours(),m=mytimestamp.getMinutes(),n=mytimestamp.getSeconds();
                    f=mytimestamp.getDay();
                    var g=mytimestamp.getDate(),h=mytimestamp.getMonth(),p=mytimestamp.getFullYear(),q="",r="";
                    "12"==b.format&&(q=" AM",11<d&&(q=" PM"),12<d&&(d-=12),0==d&&(d=12));
                    d=l(d);
                    m=l(m);
                    n=l(n);
                    "false"!=b.calendar&&(b.usenumbers?(b.langSet="nu",f=10>e[b.langSet].months[h]?"0"+e[b.langSet].months[h]:e[b.langSet].months[h],r="<span id='1' class='clockdate'>"+p+"-"+f+"-"+(10>g?"0"+g:g)+"</span>"):r="en"==b.langSet?"<span id='2' class='clockdate'>"+e[b.langSet].weekdays[f]+", "+g+" de "+e[b.langSet].months[h]+" "+p+" "+"</span>":"<span id='3' class='clockdate'>"+e[b.langSet].weekdays[f]+", "+g+" , "+e[b.langSet].months[h]+" del "+p+" "+"</span>");
                    c(k).html(r+"<span id='4' class='clocktime'>"+d+":"+m+("true"==a.seconds?":"+n:"")+q+"</span>");

                t[s]=setTimeout(function(){u(c(k),b)},1E3)}
            };u(c(this),a)}
        )};

        return this
    })(jQuery);

    //$("#clock1").clock();
    $("#clock2").clock({"calendar":"true"});
    $("#clock3").clock({"usenumbers":"true"});
</script>

<script>

    //variables globales
    var carrito = new Array();
    var total = 0;
    var mediopago = 1;
    var bandera = 1;
    var scroll = 0;

    // 1.0 - FUNCION PARA INGRESAR PRODUCTO AL HACER CLICK EN BOTON
    function ingresarProductos()
    {
        console.clear();
        console.log('INICIO AGREGAR PRODUCTO');

        var codigo;
        var contenido;
        
        codigo = document.getElementById("entrada").value;

        //PARA INICIAR Y CONTINUAR LO PRIMERO ES INGRESAR UN PRODUCTO
        if(codigo.trim() != '')
        {
            contenido = getInfo(codigo.trim());
            contenido = (jQuery.parseJSON(contenido));
            
            console.log(contenido);
            
            //SI EL CARRITO TIENE PRODUCTOS
            if(carrito && carrito.length)
            {
                var bandera=1;
                console.log("DETALLE DEL CARRITO ACTUAL :" +carrito);
                for(var i=0; i<carrito.length; i++)
                { 
                    if(carrito[i][1] == contenido.producto_codigo)
                    {
                        //carrito[i][0]= (parseInt(carrito[i][0])+1);                        
                        var stock = parseInt(contenido.producto_stock_actual)-parseInt(carrito[i][0]);
                        
                        console.log("STOCK ACTUAL DEL PRODUCTO DESCONTADO : "+stock);
                        
                        if(stock > 0 )
                        {
                            console.log("aca1");
                            bandera=0;
                            if(stock < contenido.producto_stock_minimo && stock >= 0)
                            {
                                $.Notification.notify("warning","botton rigth","Advertencia","Quedan pocos productos en stock");
                                displayResult(1,contenido.producto_nombre,contenido.producto_valor_venta,contenido.producto_codigo);    
                            }
                            else
                            {
                                displayResult(1,contenido.producto_nombre,contenido.producto_valor_venta,contenido.producto_codigo);      
                            }
                        }

                        if(stock==0)
                        {
                            bandera=0;
                            console.log("aca2");
                            $.Notification.notify("error","botton rigth","Producto no encontrado","No qudan mas productos");
                        }
                    }                                                               
                }

                if(contenido.producto_stock_actual==0)
                {
                    console.log("aca3");
                    $.Notification.notify("error","botton rigth","Producto no encontrado","Intente nuevamente o agregue el producto al stock");
                }

                if(bandera == 1 && contenido.producto_stock_actual > 0)
                {
                    console.log("aca4");
                    displayResult(1,contenido.producto_nombre,contenido.producto_valor_venta,contenido.producto_codigo);                
                }
            }
            else
            {
                if(contenido.producto_stock_actual == 0)
                {
                    console.log("aca5");
                    $.Notification.notify("error","botton rigth","Producto no encontrado","Intente nuevamente o agregue el producto al stock");
                }                    
                else
                {
                    var stock = parseInt(contenido.producto_stock_actual);

                    console.log("STOCK ACTUAL DEL PRODUCTO DESCONTADO : "+(stock - 1));
                    console.log("aca6");

                    displayResult(1,contenido.producto_nombre,contenido.producto_valor_venta,contenido.producto_codigo);             
                }
            }

            clearInputs();
            console.log('FIN AGREGAR PRODUCTO');
        }   
        else
        {
            clearInputs();
            $.Notification.notify("error","botton rigth","Ningun producto seleccionado","Intente nuevamente ingresadno un producto");
        }          
    }

    // 1.1 -  FUNCION PARA INGRESAR PRODUCTO AL PRESIONAR ENTER
    document.getElementById('entrada').onkeypress = function(e)
    {
        console.clear();
        console.log('INICIO AGREGAR PRODUCTO');

        var codigo;
        var contenido;

        if (!e) e = window.event;

        if (e.keyCode == '13')
        {
            focus();

            codigo=document.getElementById("entrada").value;                
            
            contenido=getInfo(codigo.trim());
            contenido=(jQuery.parseJSON(contenido));
            //console.log(contenido);

            //PARA INICIAR Y CONTINUAR LO PRIMERO ES INGRESAR UN PRODUCTO
            if(codigo.trim() != '')
            {
                if(carrito && carrito.length)
                {
                    var bandera=1;
                    for(var i=0;i<carrito.length;i++)
                    { 
                        if(carrito[i][1]==contenido.producto_codigo)
                        {
                            //carrito[i][0]= (parseInt(carrito[i][0])+1);                        
                            var stock = parseInt(contenido.producto_stock_actual)-parseInt(carrito[i][0]);
                            
                            console.log("STOCK ACTUAL"+stock);
                            if(stock>0 )
                            {
                                //console.log("aca1");
                                bandera=0;
                                if(stock<contenido.producto_stock_minimo && stock>=0)
                                {
                                    $.Notification.notify("warning","botton rigth","Advertencia","Quedan pocos productos en stock");
                                    displayResult(1,contenido.producto_nombre,contenido.producto_valor_venta,contenido.producto_codigo);    
                                }
                                else
                                {
                                    displayResult(1,contenido.producto_nombre,contenido.producto_valor_venta,contenido.producto_codigo);     
                                }
                            }

                            if(stock==0)
                            {
                                bandera=0;
                                //console.log("aca2");
                                $.Notification.notify("error","botton rigth","Producto no encontrado","No qudan mas productos");
                            }
                        }
                    }

                    if(contenido.producto_stock_actual==0)
                    {
                        //console.log("aca3");
                        $.Notification.notify("error","botton rigth","Producto no encontrado","Intente nuevamente o agregue el producto al stock");
                    } 

                    if(bandera==1 && contenido.producto_stock_actual>0)
                    {
                        //console.log("aca4");
                        displayResult(1,contenido.producto_nombre,contenido.producto_valor_venta,contenido.producto_codigo);
                    }
                }
                else
                {
                    if(contenido.producto_stock_actual==0)
                    {
                        //console.log("aca5");
                        $.Notification.notify("error","botton rigth","Producto no encontrado","Intente nuevamente o agregue el producto al stock");
                    }                    
                    else
                    {
                        //console.log("aca6");
                        displayResult(1,contenido.producto_nombre,contenido.producto_valor_venta,contenido.producto_codigo);                
                    }
                }

                clearInputs();
                console.log('FIN AGREGAR PRODUCTO');
            }   
            else
            {
                $.Notification.notify("error","botton rigth","Ningun producto seleccionado","Intente nuevamente ingresadno un producto");
            } 
        }
        else if(e.keyCode == '32')
        {
            console.log("TECLA ESPACIADORA PRESIONADA");
            
            if(carrito.length > 0)
            {
                clearInputs();
                $("#btn_pagar").click();
                
            }
            else
            {
                clearInputs();
                $.Notification.notify("error","botton rigth","Ningun producto seleccionado","Intente nuevamente ingresadno un producto");
            }
        }
        else
        {
            console.log("LA TECLA PRESIONADA NO EJECUTA NADA");
        }
    }

    // 2.1 -  FUNCION PARA OBTENER LOS DATOS DE UN PRODUCTO
    function getInfo(cb) 
    {
        var CBarra = cb;
        var url = '/mca/index.php/Cventas/getDatosPorductos';
        var tmp = null;

        $.ajax({
            'async': false,
            'type': "POST",
            'global': false,
            'dataType': 'html',
            'url': url,
            'data': {'CBarra':CBarra},
            'success': function (data) {
                tmp = data;
                //console.log(tmp);
            }
        });

        return tmp;
    } 

    // 2.2 -  FUNCION PARA ACTUALIZAR CARRITO
    function displayResult(a,b,c,d)
    {
        console.log('INICIO ACTUALIZANDO CARRITO, PRODUCTOS Y TOTAL');

        var Ccantidad=a;
        var Cproduto=b;
        var Cvalor=c;
        var Ccodigo=d;
        
        var bandera=0;

        if(carrito && carrito.length) 
        {
            //con datos
            for(var i=0; i<carrito.length; i++)
            { 
                console.log(carrito[i][1]);                   
                if(carrito[i][1] == Ccodigo)
                {
                    carrito[i][0]= (parseInt(carrito[i][0])+1);
                    bandera=1;
                    $('#'+Ccodigo).html(carrito[i][0]);
                    //Ccantidad=(parseInt(carrito[i][0])+1);
                }                      
            }

            if(bandera==0)
            {
                carrito.push([1,Ccodigo,Cproduto,Cvalor]);
                //carrito.push([2,Ccodigo]);
                var indice=carrito.length;
                indice=indice-1;
                //"$"+parseInt(d.proyecto_honorario_base_plano).toLocaleString(['ban', 'id'])
                document.getElementById("Tventas").insertRow(-1).innerHTML =
                '<td  width="80px" id='+Ccodigo+'>'+Ccantidad+'</td>'+
                '<td width="300px">'+Cproduto+'</td>'+
                '<td width="200px">$'+parseInt(Cvalor).toLocaleString(['ban', 'id'])+'</td>'+
                '<td>'+Ccodigo+'</td>'+
                '<td  width="30px" id=add'+Ccodigo+' align="center"><button onclick="setAgregar(`'+Ccodigo+'`)" class="btn btn-icon waves-effect waves-light btn-success m-b-5"><b> <i class="mdi ion-plus-round"></i></b></button></td>'+
                '<td  width="30px" id=sub'+Ccodigo+' align="center"><button onclick="setQuitar(`'+Ccodigo+'`,'+indice+')" class="btn btn-icon waves-effect waves-light btn-warning m-b-5"><b> <i class="mdi ion-minus-round"></i></b></button></td>'+
                '<td  width="30px" id=del'+Ccodigo+' align="center"><button onclick="setEliminar('+indice+')" class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></td>';
                //$('tr:not(:nth-child(-1)):visible:last').hide(); // hide the last visible tr but not the first one
               // id_num = $('tr:visible:last').attr('id') ? $('tr:visible:last').attr('id') : 0; 
               $('#listtableWrapperScroll').scrollTo(100);
            }
        }
        else
        {
            //vacio
            carrito.push([1,Ccodigo,Cproduto,Cvalor]);
            //carrito.push([2,Ccodigo]);
            document.getElementById("Tventas").insertRow(-1).innerHTML =
            '<td  width="80px" id='+Ccodigo+'>'+Ccantidad+'</td>'+
            '<td width="300px">'+Cproduto+'</td>'+
            '<td width="200px">$'+parseInt(Cvalor).toLocaleString(['ban', 'id'])+'</td>'+
            '<td>'+Ccodigo+'</td>'+
            '<td  width="30px" id=add'+Ccodigo+' align="center"><button onclick="setAgregar(`'+Ccodigo+'`)" class="btn btn-icon waves-effect waves-light btn-success m-b-5"><b> <i class="mdi ion-plus-round"></i></b></button></td>'+
            '<td  width="30px" id=sub'+Ccodigo+' align="center"><button onclick="setQuitar(`'+Ccodigo+'`,0)" class="btn btn-icon waves-effect waves-light btn-warning m-b-5"><b> <i class="mdi ion-minus-round"></i></b></button></td>'+
            '<td  width="30px" id=del'+Ccodigo+' align="center"><button onclick="setEliminar(0)"class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></td>';


            $('#listtableWrapperScroll').scrollTo(100); 
           // $('tr:not(:nth-child(1)):visible:last').hide();
            //id_num = $('tr:visible:last').attr('id') ? $('tr:visible:last').attr('id') : 0; 
        }
        //carrito.push([1,Ccodigo]);
        console.log(carrito);
        console.log('FIN ACTUALIZANDO CARRITO, PRODUCTOS Y TOTAL');
        //console.log(carrito[0]);
        //console.log(carrito[0][1]);
        // console.log(Ccodigo);
        getTotal();
    }

    //2.3 FUNCION PARA ACTUALIZAR TOTAL
    function getTotal() 
    {
        console.log("INICIO CALCULO TOTAL");
        console.log("CARRITO :"+carrito);
        total = 0;

        //SE RECORRE EL CARRITO
        for(var i=0; i<carrito.length; i++)
        {
            var contenido=getInfo(carrito[i][1]);
            contenido=(jQuery.parseJSON(contenido));
    
            console.log("CONTENIDO CARRITO ->PRODUCTO :"+JSON.stringify(contenido));

            //OBTENGO CANTIDAD DEL MISMO PRODUCTO
            var cantidad = parseInt(carrito[i][0]);
            console.log("CANTIDAD DEL MISMO PRODUCTO : "+ cantidad);
            
            //RECORRO
            console.log((carrito[i][1]) +"=="+ String(contenido.producto_codigo));
            console.log("CARRITO CANTIDAD: "+carrito[i][1] + "  VALOR VENTA : "+contenido.producto_valor_venta);
            
            var total_producto =  cantidad * parseInt(contenido.producto_valor_venta);
            total=total+total_producto;
            console.log("TOTAL :"+total);
            console.log("ciclos : "+(i+1));
        }

        $("#total").html("Total: $"+parseInt(total).toLocaleString(['ban','id']));

        console.log("FIN CALCULO TOTAL");
    }

    //3.0 FUNCION PAGAR
    //NO SE POR QUE ESTABA -> async function ingresarPago()
    function ingresarPago()
    {
        console.log("INICIO INGRESAR PAGO");

        if(carrito.length > 0)
        {
            //BLOQUEAR MODAL PARA ESPERAR UNA RESPUESTA POR PARTE DEL USUARIO
            $('#modalDetallePago').modal({backdrop: 'static', keyboard: true})
            
            var quitar = document.getElementById("entrada");
            quitar.classList.remove("scanner");
            
            var voleta='<table class="table table-bordered">'+
                            '<thead class="table-active">'+
                                '<tr>'+
                                    '<td><b>Producto</b></td>'+
                                    '<td><b>Cantidad</b></td></tr>'+
                            '</thead>'+
                        '<tbody>';

            for(var i=0;i<carrito.length;i++)
            {
                var carro=getInfo(carrito[i][1]);
                carro=(jQuery.parseJSON(carro));
                voleta = voleta+'<tr><td>'+carro["producto_nombre"]+'</td>'+'<td>X'+carrito[i][0]+'</td></tr>'; 
            }

            voleta=voleta+'</tbody>'+'</table>';
            $('#contenido_modal_detalle').html(voleta);
            $('#modalDetallePago').modal('toggle');

        }
        else
        {
            //NO HACE NADA
            console.log('EL CARRITO ESTA VACIO');
            $.Notification.notify("error","botton rigth","Ningun producto seleccionado","Intente nuevamente ingresadno un producto");
        }
        
        console.log("FIN INGRESAR PAGO");
    }


    //Funciones botones
    function setAgregar(y) 
    { 
        var ad=y;       
        cont=getInfo(""+ad+"");
        cont=(jQuery.parseJSON(cont));
        console.log("AGREGAR");

        for(var i=0;i<carrito.length;i++)
        { 
            if(carrito[i][1]==cont.producto_codigo)
            {                      
                var stock = parseInt(cont.producto_stock_actual)-parseInt(carrito[i][0]);
                if(stock>0 )
                {
                    bandera=0;
                    if(stock<cont.producto_stock_minimo && stock>=0)
                    {
                        $.Notification.notify("warning","botton rigth","Advertencia","Quedan pocos productos en stock");
                       displayResult(1,cont.producto_nombre,cont.producto_valor_venta,ad);    
                    }
                    else
                    {
                        displayResult(1,cont.producto_nombre,cont.producto_valor_venta,ad);    
                    }                                
                }

                if(stock==0)
                {
                    bandera=0;
                    //console.log("aca2");
                    $.Notification.notify("error","botton rigth","Producto no encontrado","No qudan mas productos");
                }
            }                                                               
        }
        //getTotal();
    } 

    function setQuitar(z,w) 
    {
        console.log("QUITAR");
        contenido=getInfo(""+z+"");
        contenido=(jQuery.parseJSON(contenido));
        for(var i=0;i<carrito.length;i++){ 
            if(carrito[i][1]==contenido.producto_codigo){                      
                var stock = parseInt(carrito[i][0]);
                console.log(stock);
                stock = stock - 1;
                if(stock>0 ){
                    carrito[i][0] = stock;  
                    $('#'+z).html(carrito[i][0]);                     
                }
                else{
                    setEliminar(w);
                }
            }                                                               
        }
        getTotal();
    }

    function setEliminar(x)
    {
        if (confirm('Confirmar acción'))
        {
            console.log("ELIMINAR"+x);
            $('#Tventas tr:eq('+x+')').remove();
            carrito.splice(x, 1);
            getTotal();
        }
        else
        {
            // Do nothing!
        }
       
    }

    //FUNCION PARA LIMPIAR CODIGO DE PRODUCTO
    function clearInputs()
    {
        console.log('limpiar campo de entrada');
        $("#entrada").val('');
    }

    function cerrarPago()
    {
        console.log("INICIO CERRAR PAGO");
        $('#entrada').prop('autofocus');
        var agregar = document.getElementById("entrada");
        agregar.classList.add("scanner"); 
        console.log("FIN CERRAR PAGO");
        focus();         
    }

    function enviarPago()
    {  
        console.log("INICIO ENVIO DE PAGO");   

        var url = '/mca/index.php/Cventas/realizarPago'; 
        // var htmlString = document.getElementById("total").html();
        var to=document.getElementById("total").innerHTML;
        console.log(to);    
        
        $.ajax({
            'async': false,
            'type': "POST",
            'global': false,
            'dataType': 'html',
            'url': url,
            'data': {'productos':carrito , 'medio':mediopago, 'total':total},
            'success': function (data) 
            {
                imprimir_documento(data);
                resetear_venta();
            }
        });

        //window.print("hola");
    }
   

    function imprimir_documento(data)
    {
        console.log("inicio imprimir");
        var ventana = window.open("<?php echo base_url();?>Cboleta/genera_dte/"+data)
        ventana.print();

        setTimeout(function () {
            ventana.close(); 
            }, 5000);
    }

    //
    function resetear_venta()
    {
        carrito = new Array();
        total = 0;
        mediopago = 1;
        bandera = 1;
        document.getElementById("Tventas").innerHTML="";
        $("#total").html("Total: $0");
        //oculto el modal
        $("#modalDetallePago").modal('hide');
        //regreso el foco
        focus();
    }

    document.getElementById("id_body").onkeypress = function(e)
    {
        if (!e) e = window.event;

        if (e.keyCode == '13')
        {
            console.log("FOCO");
            focus();
        }
    }

    //FUNCION QUE CONTROLA EL FOCO DE LA ENTRA DE PRODUCTO
    function focus()
    {   
        console.log("FOCO INICIADO");
        // Focus al cargar
        $('.scanner').focus();
        // // Forzar focus  
    }

    //FUNCION QUE CONTROLA LA SALIDA DEL FOCO Y EN ESTE CASO VUELVA A ENTRAR AL FOCO DA PROBLEMAS SOLUCIONARRRRR!!!
    // $('.scanner').focusout(function()
    // {
    //     console.log("FOCO REINICIADO");
    //     focus();
    // });

    //EJECUTA LA FUNCION FOCO
    focus();

    $("#modalDetallePago").on('hidden.bs.modal', function () 
    {
        console.log("INICIO ACCION CIERRE MODAL");
        var quitar = document.getElementById("entrada");
        quitar.classList.add("scanner");
        focus();
    });

    $('input[type=radio][name=radio]').change(function() 
    {
        if (this.value == 'option1') 
        {
            mediopago=1;
        }
        else if (this.value == 'option2') 
        {
            mediopago=2;
        }
        else if (this.value == 'option3') 
        {
            mediopago=3;
        }
    });
</script>

<style>
    td{
        text-align:center;
	}    
</style>