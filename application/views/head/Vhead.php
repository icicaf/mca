<head>
    <meta charset="utf-8" />
    <title>MCA - PuntoVenta</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="Sistema de venta" name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/images/favicon.ico">

    <!-- App css -->
    <link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>/assets/css/jquery.circliful.css" rel="stylesheet" type="text/css" />
    
    <script src="<?php echo base_url(); ?>/assets/js/modernizr.min.js"></script>

    <!-- jQuery  -->
    <script src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script>

</head>