<div class="card-box">
	<h4 class="header-title m-t-0">RESUMEN DE VENTAS <h6>Calculo realizado al <cite title="Source Title"> <?php print_r($hoy) ?></cite></h6></h4>
    <div class="row">
        <div class="col-lg-4 col-md-6">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-success pull-left">
                    <i class="mdi mdi-cart text-pink"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-success m-t-10">$ <span class="counter">
                        <?php 
                            if(isset($venta_diaria[0]["venta_diaria"])){
                                print_r(number_format($venta_diaria[0]["venta_diaria"],0,",","."));
                            }else{
                                echo "0";
                            }
                        ?></span></h3>
                    <p class="text-muted text-nowrap m-b-10">Total Vendido Hoy</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-primary pull-left">
                    <i class="mdi mdi-cart text-pink"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-primary m-t-10">$ <span class="counter">
                    <?php 
                        if(isset($venta_semanal[0]["venta_semanal"])){
                            print_r(number_format($venta_semanal[0]["venta_semanal"],0,",","."));
                        }else{
                            echo "0";
                        }
                    ?></span></h3>
                    <p class="text-muted text-nowrap m-b-10">Total Vendido en la Semana</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-primary pull-left">
                    <i class="mdi mdi-cart text-pink"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-primary m-t-10">$ <span class="counter">
                    <?php 
                        if(isset($venta_mensual[0]["venta_mensual"])){
                            print_r(number_format($venta_mensual[0]["venta_mensual"],0,",","."));
                        }else{
                            echo "0";
                        }
                    ?></span></h3>
                <p class="text-muted text-nowrap m-b-10">Total Vendido en el Mes</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>