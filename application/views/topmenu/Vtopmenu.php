<div class="navbar-custom">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                <li class="has-submenu">
                    <a id="moduloInicio_resumen" href="javascript:void(0);"><i class="ti-home"></i>Inicio</a>
                </li>

                <?php 
                    if('VENTAS')
                    {
                        ECHO ' <li class="has-submenu">
                                <a href="#" onclick="return false;"><i class="fa fa-dollar"></i>Ventas</a>
                                <ul class="submenu">
                                    <li id="moduloVenta_realizarVenta"><a href="javascript:void(0);">Nueva Venta</a></li>
                                </ul>
                            </li>';
                    }
 
                    if('PRODUCTOS')
                    {
                        ECHO '<li class="has-submenu">
                                <a href="#" onclick="return false;"><i class="ti-menu"></i>Productos</a>
                                <ul class="submenu">
                                    <li id="moduloPoducto_escanearProducto"><a href="javascript:void(0);">Registrar Productos</a></li>
                                    <li id="moduloPoducto_ListadoProducto"><a href="javascript:void(0);">Listado de Productos</a></li>
                                    <li id="moduloPoducto_Inventario"><a href="javascript:void(0);">Inventario</a></li>
                                </ul>
                            </li>';
                    }

                    if('OTRAFUNCIONALIDAD')
                    {
                        ECHO ' <li class="has-submenu" onclick="return false;">
                                <a href="#" onclick="return false;"><i class="ti-spray"></i>Otra funcionalidad</a>
                                <ul class="submenu">
                                    <li class="has-submenu">
                                        <a href="javascript:void(0);">Reportes</a>
                                        <ul class="submenu">
                                            <li><a href="javascript:void(0);">Productos</a></li>
                                            <li><a href="javascript:void(0);">Compras</a></li>
                                            <li><a href="javascript:void(0);">Ventas</a></li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#" onclick="return false;">Usuarios</a>
                                        <ul class="submenu">
                                            <li id="moduloUsuarios_nuevoUsuario"><a href="javascript:void(0);">Nuevo Usuario</a></li>
                                            <li id="moduloUsuarios_gestionUsuario"><a href="javascript:void(0);">Gestión Usuario</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>';
                    }

                    if('PRODUCTOS')
                    {
                        ECHO '<li class="has-submenu" onclick="return false;" hidden>
                            <a href="javascript:void(0);"><i class="ti-gift"></i>Extras</a>
                            <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        <li><a href="extras-profile.html">Profile</a></li>
                                        <li><a href="extras-team.html">Team Members</a></li>
                                        <li><a href="extras-timeline.html">Timeline</a></li>
                                        <li><a href="extras-invoice.html">Invoice</a></li>
                                        <li><a href="extras-calendar.html"> Calendar</a></li>
                                        <li><a href="extras-email-template.html">Email template</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <ul>
                                        <li><a href="extras-maintenance.html">Maintenance</a></li>
                                        <li><a href="extras-coming-soon.html">Coming-soon</a></li>
                                        <li><a href="extras-gallery.html"> Gallery</a></li>
                                        <li><a href="extras-pricing.html">Pricing</a></li>
                                        <li><a href="extras-faq.html">FAQ</a></li>
                                        <li><a href="extras-treeview.html">Treeview</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>';
                    }
                ?>
            </ul>
            <!-- End navigation menu -->
        </div> <!-- End #navigation -->
    </div> <!-- End container -->
</div> <!-- End navbar-custom -->