<script src="<?php echo base_url(); ?>/assets/js/popper.min.js"></script><!-- Popper for Bootstrap --><!-- Tether for Bootstrap -->
<script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/waves.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/jquery.scrollTo.min.js"></script>

<!-- Custom main Js -->
<script src="<?php echo base_url(); ?>/assets/js/jquery.core.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/jquery.app.js"></script>

<!-- Notificaciones Pop -->
<script src="<?php echo base_url(); ?>/assets/js/notify.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/notify-metro.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/jquery.blockUI.js"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url(); ?>/assets/js/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="<?php echo base_url(); ?>/assets/js/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/datatables/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/datatables/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/datatables/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/datatables/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/datatables/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/datatables/buttons.print.min.js"></script>
<!-- Key Tables -->
<script src="<?php echo base_url(); ?>/assets/js/datatables/dataTables.keyTable.min.js"></script>
<!-- Responsive examples -->
<script src="<?php echo base_url(); ?>/assets/js/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/datatables/responsive.bootstrap4.min.js"></script>
<!-- Selection table -->
<script src="<?php echo base_url(); ?>/assets/js/datatables/dataTables.select.min.js"></script>

<!-- Script Graficos INICIO -->
<script src="<?php echo base_url(); ?>/assets/js/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/jquery.circliful.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/skycons.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/morris.min.js"></script>



<script type="text/javascript">
function alterna_modo_de_pantalla() {
  if ((document.fullScreenElement && document.fullScreenElement !== null) ||    // metodo alternativo
      (!document.mozFullScreen && !document.webkitIsFullScreen)) {               // metodos actuales
    if (document.documentElement.requestFullScreen) {
      document.documentElement.requestFullScreen();
    } else if (document.documentElement.mozRequestFullScreen) {
      document.documentElement.mozRequestFullScreen();
    } else if (document.documentElement.webkitRequestFullScreen) {
      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
    }
  } else {
    if (document.cancelFullScreen) {
      document.cancelFullScreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    }
  }
}
document.addEventListener("keyup", keyUpTextField, false);
function keyUpTextField (e) 
{
    var keyCode = e.keyCode;
    if(keyCode==122){/*ejecutaAlgo*/alterna_modo_de_pantalla();}
}   
</script>