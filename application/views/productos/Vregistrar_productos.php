<div class="card-box">
    <h4 class="header-title m-t-0">Registrar Producto</h4>
    <p class="text-muted font-14 m-b-20">
        Ingrese los datos que se solicitaran a continuación para el correcto ingreso del producto.
    </p>
    <div class="form-group row">
        <div class="col-sm-2"></div>

        <div class="col-sm-8">
            <form id="formulario_agregar_producto" name="formulario_agregar_producto">
                <?php
                    //print_r($existencia);
                    $btn_accion = "";
                    $btn_deshabilita = "";
                    if($existencia==0) {
                        $btn_accion = "Ingresar";
                        $btn_deshabilita = "hidden";
                        $btn_deshabilita_val = "";
                    } else if($existencia==1){
                        $btn_accion = "Actualizar";
                        $btn_deshabilita = "";
                        $btn_deshabilita_val = "Deshabilitar Producto";
                    } else if($existencia==2){
                        $btn_accion = "Habilitar";
                        $btn_deshabilita = "hidden";
                        $btn_deshabilita_val = "";
                    }

                    $tipo_producto_id = "";
                    $tipo_producto_nombre = "";
                    $nombre = "";
                    $descri = "";
                    $documento = "";
                    $compra = "";
                    $venta = "";
                    $stock_minimo = "";
                    $stock_actual = "";
                    $tipo_producto = "";

                    if($existencia==1 || $existencia==2){
                        foreach ($datos_producto AS $key) {
                            $tipo_producto_id = $key["tipo_producto_id"];
                            $tipo_producto_nombre = $key["tipo_producto_nombre"];
                            $nombre = $key["producto_nombre"];
                            $descri = $key["producto_descripcion"];
                            $compra = $key["producto_valor_compra"];
                            $venta = $key["producto_valor_venta"];
                            $stock_minimo = $key["producto_stock_minimo"];
                            $stock_actual = $key["producto_stock_actual"];
                            $tipo_producto = $key["tipo_producto_id"];
                        }
                    } else {
                        $tipo_producto_id = "";
                        $tipo_producto_nombre = "";
                        $nombre = "";
                        $descri = "";
                        $compra = "";
                        $venta = "";
                        $stock_minimo = "";
                        $stock_actual = "";
                        $tipo_producto = "";
                    }
                    echo '
                        <div class="form-group row">
                            <label for="exampleInputName2" class="col-4 col-form-label">Codigo del Producto <span class="text-danger">*</span></label>
                            <div class="col-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="mdi mdi-barcode-scan"></i></span>
                                    <input style="font-weight: bold;" type="text" class="form-control" id="product_code" name="product_code" placeholder="..." value="'.$product_code.'" disabled>
                                </div>
                            </div>
                        </div>
             
                        <div class="form-group row">
                            <label for="exampleInputName2" class="col-4 col-form-label">Nombre del Producto <span class="text-danger">*</span></label>
                            <div class="col-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="mdi mdi-rename-box"></i></span>
                                    <input type="text" class="form-control" id="product_name" name="product_name" placeholder="..." value="'.$nombre.'" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-4 col-form-label">Descripción del Producto <span class="text-danger">*</span></label>
                            <div class="col-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="mdi mdi-rename-box"></i></span>
                                    <textarea class="form-control" rows="3" id="product_description" name="product_description" required>'.$descri.'</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="exampleInputName2" class="col-4 col-form-label">Documento Compra <span class="text-danger">*</span></label>
                            <div class="col-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="mdi mdi-currency-usd"></i></span>
                                    <input type="number" class="form-control currency" id="product_document" name="product_document" placeholder="..." value="'.$documento.'" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="exampleInputName2" class="col-4 col-form-label">Valor Compra <span class="text-danger">*</span></label>
                            <div class="col-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="mdi mdi-currency-usd"></i></span>
                                    <input type="number" class="form-control currency" id="product_value_purchase" name="product_value_purchase" placeholder="..." value="'.$compra.'" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="exampleInputName2" class="col-4 col-form-label">Valor Venta <span class="text-danger">*</span></label> 
                            <div class="col-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="mdi mdi-currency-usd"></i></span>
                                    <input type="number" class="form-control currency" id="product_value_sale" name="product_value_sale" placeholder="..."value="'.$venta.'" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="exampleInputName2" class="col-4 col-form-label">Cantidad de Stock <span class="text-danger">*</span></label>
                            <div class="col-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="mdi mdi-numeric"></i></span>
                                    <input type="number" class="form-control" id="product_stock" name="product_stock" placeholder="..." required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="exampleInputName2" class="col-4 col-form-label">Stock Minimo <span class="text-danger">*</span></label>
                            <div class="col-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="mdi mdi-numeric"></i></span>
                                    <input type="number" class="form-control" id="product_stock_min" name="product_stock_min" placeholder="..." value="'.$stock_minimo.'" required>
                                 </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="exampleInputName2" class="col-4 col-form-label">Stock Actual <span class="text-danger">*</span></label>
                            <div class="col-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="mdi mdi-numeric"></i></span>
                                    <input type="number" class="form-control" id="product_stock_actual" name="product_stock_actual" placeholder="..." value="'.$stock_actual.'" disabled>
                                 </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-4 col-form-label">Sección Tipo Producto</label>
                            <div class="col-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="mdi mdi-chevron-double-down"></i></span>
                                    <select class="form-control input-sm text-center text-uppercase" id="select_tipo_producto" name="select_tipo_producto" required>
                                        <option value='.$tipo_producto_id.'>'.$tipo_producto_nombre.'</option>';
                                            foreach ($tipos_productos as $key) {
                                                echo '<option value="'.$key["tipo_producto_id"].'" >'.$key["tipo_producto_nombre"].'</option>';
                                            }
                                            echo '
                                    </select>
                                </div>
                            </div>
                        </div>
                    
                        <div class="form-group row">
                            <div class="col-8 offset-4">
                                <button type="button" class="btn btn-primary waves-effect waves-light" id="btn_guardar_producto" name="btn_guardar_producto">'.$btn_accion.'</button>
                                <button type="reset" class="btn btn-secondary waves-effect m-l-5">Limpiar</button>
                                <button type="button" class="btn btn-warning waves-effect m-l-5" id="btn_deshabilita_producto" name="btn_deshabilita_producto" '.$btn_deshabilita.'>'.$btn_deshabilita_val.'</button>
                            </div>
                        </div>

                        <input type="number" class="form-control" id="id_product" name="id_product" value="'.$product_ID.'" disabled hidden>
                    ';
                ?>
            </form>
        </div>

        <div class="col-sm-2"></div>
    </div>
</div>
<br>
<br>

<script>
    //////////////////////////////////////////////////////////////////////////////////////////////////
    $(document).ready(function() 
    {
        $.valida_formulario_ingreso_datos_producto = function(){
            var errores = 0;
            $("#formulario_agregar_producto").find("select").each(function () {
                var nombre_elemento = $(this).attr("name");
                if($(this).prop('required')) {
                    if($(this).val().length < 1) {
                        console.log('ERRORES : '+nombre_elemento);
                        errores++;
                    }
                }
            });
            $("#formulario_agregar_producto").find("input").each(function () {
                var nombre_elemento = $(this).attr("name");
                if($(this).prop('required')) {
                    if($(this).val().length < 1) {
                        console.log('ERRORES : '+nombre_elemento);
                        errores++;
                    }
                }
            });
            return errores;
        }
        
        $("#btn_guardar_producto").click(function(){
            if($.valida_formulario_ingreso_datos_producto() == 0){
                console.log($.valida_formulario_ingreso_datos_producto());
                $.guardar_datos_producto();
            }else{
                $.Notification.notify('error','bottom right','COMPLETE FORMULARIO', 'Debe ingresar todos los datos para el correcto ingreso del producto.');
                console.log($.valida_formulario_ingreso_datos_producto());
            }
        });

        $.guardar_datos_producto = function(){
            var formAddProducto = $('#formulario_agregar_producto');
            //Busca los input deshabilitados y les remueve esa propieda
            var disabled = formAddProducto.find(':input:disabled').removeAttr('disabled');
            //Serializa el formulario para todos los input
            var serialized = formAddProducto.serialize().split('=');
            //re-disabled the set of inputs that you previously enabled
            //disabled.attr('disabled','disabled');

            //Muestra el formulario que se esta enviando
            var arreglo = new Array();
            var datos_formulario = new FormData(document.forms.namedItem("formulario_agregar_producto"));
            for (var pair of datos_formulario.entries()) {
                console.log('Elemento: '+pair[0]+ ', Valor: ' + pair[1]);
                arreglo.push(pair[0]+'='+pair[1]);
            }
            console.log(arreglo);

            var url = '/mca/index.php/Cproductos/ingresar_producto';
            $.ajax({
                type: "POST",
                url: url,
                data: { codigo:arreglo[0].split('=')[1],
                        nombre:arreglo[1].split('=')[1],
                        descri:arreglo[2].split('=')[1],
                        documento:arreglo[3].split('=')[1],
                        compra:arreglo[4].split('=')[1],
                        venta:arreglo[5].split('=')[1],
                        stock_ingreso:arreglo[6].split('=')[1],
                        stock_minimo:arreglo[7].split('=')[1],
                        stock_actual:arreglo[8].split('=')[1],
                        tipo_producto:arreglo[9].split('=')[1],
                        id_producto:arreglo[10].split('=')[1] },
                success: function(msg) {
                    console.log(msg);
                    console.log("Datos Ingresados");
                    var url = '/mca/index.php/Cproductos/vista_escanear_producto';
                    $("#bodycentral").load(url, function(response,status, xhr) {});
                    $.Notification.notify('success','bottom right','DATOS INGRESADOS', 'Se han guardado los datos Correctamente.');
                },
                error: function() {
                    console.log("error");
                }
            });
        }

        $('#btn_deshabilita_producto').click(function(){
            var idProducto = document.getElementById("id_product").value;
            console.log("ID producto a deshabilitar"+idProducto);

            var url = '/mca/index.php/Cproductos/deshabilitar_producto';
            $.ajax({
                type: "POST",
                url: url,
                data: {idProducto:idProducto},
                success: function(msg) {
                    console.log(msg);
                    console.log("Datos Deshabilitados");
                    var url = '/mca/index.php/Cproductos/vista_escanear_producto';
                    $("#bodycentral").load(url, function(response,status, xhr) {});
                    $.Notification.notify('success','bottom right','DATOS ELIMINADOSDESHABILITADOS', 'Se han deshabilitado los datos Correctamente.');
                },
                error: function() {
                    console.log("error");
                }
            });
        });
    //////////////////////////////////////////////////////////////////////////////////////////////////
    });
</script>