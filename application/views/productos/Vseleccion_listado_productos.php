<div class="form-group row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="header-title m-t-0">REVISAR LISTADO DE PRODUCTOS</h4>
            <p class="text-muted font-14 m-b-20">
                Seleccione el Tipo de Productos que desea revisar..
            </p>

            <form onsubmit="return false" class="form-horizontal" role="form">
                <div class="form-group row">
                    <label class="col-3 col-form-label">Sección Tipo Producto</label>
                    <div class="col-3">
                    	<div class="input-group">
                            <span class="input-group-addon"><i class="mdi mdi-chevron-double-down"></i>&nbsp; <b>TIPO</b></span>
                    		<select class="form-control input-sm text-center text-uppercase" id="select_tipo_producto" name="select_tipo_producto" required="">
                        		<option value="0" selected>Seleccionar</option>
                                <?php    
                                    foreach ($tipos_productos as $key) {
                                        echo '<option value="'.$key["tipo_producto_id"].'" >'.$key["tipo_producto_nombre"].'</option>';
                                    }
                                ?>
                            </select>
                    	</div>
                    </div>
                    <div class="col-3">
	                    <button type="button" class="btn btn-success waves-effect waves-light" id="btn_filtrar_listado" ><i class="ti-filter"></i> Filtrar</button>
	                    <button type="button" class="btn btn-danger waves-effect waves-light" id="btn_listado_completo"><i class="ti-search"></i> Todos</button>
	                </div>

                </div>                
	        </form> 
        </div>
    </div>
    
    <!-- DIV DE PANEL DE TABLAS DE REGISTROS -->
    <div class="panel panel-default col-12" id="panel_tabla_cargada" name="panel_tabla_cargada">
        <h4></h4>
        <?php
            if(isset($tabla_productos)){   
                echo $tabla_productos;
        	}
        ?>
    </div>
</div>

<br>
<br>

<script>
    $('#btn_filtrar_listado').click(function(evento) {
        var tipo = $('#select_tipo_producto').val();
        if(tipo == "" || tipo == "0"){
           console.log("SELECCIONE TIPO");
           $.Notification.notify('error','bottom right','SELECCIONE', 'Debe seleccionar el Tipo de Productos a revisar.');
        }
        else{
            $.get_listado_productos_segun_tipo(tipo);
        }  
    });

    $.get_listado_productos_segun_tipo = function(tipo) {
        console.log("ENVIADO: "+tipo);
        var controlador = '/mca/index.php/Cproductos/get_listado_productos_segun_tipo';
        $.ajax({
            type: "POST",
            url: controlador,
            data: { tipo : tipo },
            success: function(msg) {
                $("#panel_tabla_cargada").html(msg);
                $.Notification.notify('success','bottom right','OK', 'Listado de Productos');
            },
            error: function(msg) {
                console.log(msg);
            }
        });
    }
</script>