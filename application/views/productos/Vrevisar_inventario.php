<div class="card-box">
	<h4 class="header-title m-t-0">INVENTARIO DE PRODUCTOS POR DESCRIPCION <h6>Calculo realizado al <cite title="Source Title"><?php $hoy = date("j/n/Y"); echo $hoy;?></cite></h6></h4>
    <p class="text-muted font-14 m-b-20">Seleccione el detalle al cual desea acceder.</p>
	<div class="col-12">
    	<div class="form-group row">                
			<?php
				//print_r($cantidad_productos_tipo);
				$i=0;
				foreach ($cantidad_productos_tipo AS $key) {
					$descripcion = strtoupper($key['tipo_producto_nombre']);
					echo '
							<div class="col-sm-4 col-xs-12">
                        		<div class="card m-b-20">
                            		<div class="card-header bg-primary">
		                               <b>'.$descripcion.'</b>
		                            </div>
		                            <div class="card-body">
		                                <blockquote class="card-bodyquote">
		                                    <p><b>'.$key['cant_productos'].'</b> Productos en existncia dentro del inventario.</p>
		                                    <button type="button" 
		                                    		class="btn btn-warning waves-effect waves-light" 
		                                    		data-toggle="modal" 
		                                    		data-target="#modalDetalleTipoProducto"
		                                    		name="btn_cant_prod_tipo" 
				                                    id="btn_cant_prod_tipo_'.$i.'" 
				                                    value="'.$key['cant_productos'].'"
				                                    tipo_producto="'.$key['tipo_producto_id'].'"
				                                    tipo_nombre="'.$descripcion.'">Detalle</button>
		                                </blockquote>
		                            </div>
		                        </div>
		                    </div>
					';
					$i++;
				}
			?>
		</div>
	</div>
</div>

<div id="modalDetalleTipoProducto" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalDetalleTipoProducto" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalDetalleTipoProducto">Detalle de Inventario</h4>
            </div>
            <div class="modal-body" id="contenido_modal_detalle">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function(){
    //////////////////////////////////////////////////////////////////////////////////////////////////
		$('button[name="btn_cant_prod_tipo"]').click(function(){
	        var cant_productos = $(this).attr('value');
	        var id_tipo_producto = $(this).attr('tipo_producto');
	        var descripcion = $(this).attr('tipo_nombre');
	        console.log("BTN:"+cant_productos);

	        $.Notification.notify('success','bottom right','CANTIDAD DE PRODUCTOS', cant_productos+' Productos.');

	        var url = '/mca/index.php/Cproductos/get_detalle_tipo_productos';
	        $.ajax({
                type: "POST",
                url: url,
                data: {id_tipo_producto:id_tipo_producto},
                success: function(msg) {
                    carga_modal_detalle_tipo_mejoramiento(msg);
                },
                error: function() {
                    console.log("error");
                }
            });
	    });
        function carga_modal_detalle_tipo_mejoramiento(arreglo){
            $('#contenido_modal_detalle').html(arreglo);
            $('#modalDetalleTipoProducto').modal('toggle');
        }
    //////////////////////////////////////////////////////////////////////////////////////////////////
	});
</script>