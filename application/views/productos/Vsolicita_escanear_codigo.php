<div class="card-box">
    <h4 class="header-title m-t-0">Registrar Producto</h4>
    <p class="text-muted font-14 m-b-20">
        Escanee o Digite el producto que desea ingresar al sistema.
    </p>

    <div class="form-group row">
        <div class="col-sm-2"></div>
        <div class="col-8">
            <form id="formulario_escanear_producto" name="formulario_escanear_producto">
                <div class="form-group row">
                    <div class="col-12">
                        <div class="input-group">
                            <span class="input-group-addon"><b> Codigo del Producto &nbsp;</b> <i class="mdi mdi-barcode-scan"></i></span>
                            <input type="text" class="form-control scanner" id="scan_product_code" name="scan_product_code" placeholder="..." autofocus>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>
<br>
<br>

<script>
    //////////////////////////////////////////////////////////////////////////////////////////////////
    $(function() {
        // Focus al cargar
        $('.scanner').focus();
        // Forzar focus
        $('.scanner').focusout(function(){ $('.scanner').focus(); }); 
    });

    document.getElementById('scan_product_code').onkeypress = function(e){
    if (!e) e = window.event;
    if (e.keyCode == '13'){
        product_code=document.getElementById("scan_product_code").value;
        var url = '/mca/index.php/Cproductos/verifica_existencia_codigo';
        $.ajax({
            type: "POST",
            url: url,
            data: {product_code:product_code},
            success: function(respuesta){
                $.analiza_verifica_existencia_codigo(respuesta,product_code);
            },
            error: function() {
                console.log("error");
            }
        });
        return false;
        }
    }

    $.analiza_verifica_existencia_codigo = function(respuesta,product_code){
        console.log("Codigo Producto:"+product_code+" Respuesta:"+respuesta);
        if(respuesta==0){
            console.log("No Existe el articulo");
            $.Notification.notify('error','bottom right','No Existe Este Producto', 'Complete los campos para Ingresar.')
            var vista = '/mca/index.php/Cproductos/vista_formulario_producto';
            $.carga_vista_formulario_producto(product_code,0);
        } else if(respuesta==1){
            console.log("Existe el articulo");
            $.Notification.notify('success','bottom right','PRODUCTO EXISTENTE', 'Complete los campos a modificar');
            $.carga_vista_formulario_producto(product_code,1);
        }else if(respuesta==2){
            console.log("Existe el articulo, Estado Deshabilitado");
            $.Notification.notify('warning','bottom right','PRODUCTO DESHABILITADO', 'Complete los campos a modificar');
            $.carga_vista_formulario_producto(product_code,2);
        }
    }

    $.carga_vista_formulario_producto = function(product_code,existencia){
        var vista = '/mca/index.php/Cproductos/vista_formulario_producto';
        $.ajax({
            type: "POST",
            url: vista,
            data: {product_code:product_code,
                existencia:existencia},
            success: function(vistaForm) {
                 $("#bodycentral").html(vistaForm);
            },
            error: function() {
                console.log("error");
            }
        });
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
</script>