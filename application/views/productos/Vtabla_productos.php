<div class="card-box table-responsive">
    <h4 class="m-t-0 header-title">LISTADO DE PRODUCTOS EN EXISTENCIA</h4>
    <p class="text-muted font-14 m-b-30">
        Listado de los productos con o sin stock dentro del sistema, en caso de eliminar algun producto del sistema, clickear en el boton rojo.
    </p>

    <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" role="grid" aria-describedby="datatable-buttons_info" style="width: 100%;">
        <thead>
            <tr>
                <th class="info" style="text-align:center;">#</th>
                <th class="info" style="text-align:center;">Codigo Producto</th>
                <th class="info" style="text-align:center;">Nombre Producto</th>
                <th class="info" style="text-align:center;">Descripcion Producto</th>
                <th class="info" style="text-align:center;">Ultimo Valor Compra</th>
                <th class="info" style="text-align:center;">Valor Venta</th>
                <th class="info" style="text-align:center;">Stock Actual</th>
                <th class="info" style="text-align:center;">Stock Minimo</th>
                <th class="info" style="text-align:center;">Tipo de Producto</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $i=1;
                foreach($tabla_productos as $key){
                    echo '
                        <tr id="fila_'.$i.'" value="'.$i.'" style="text-align:center;">
                            <td>'.$i.'</td>
                            <td>'.$key['producto_codigo'].'</td>
                            <td>'.$key['producto_nombre'].'</td>
                            <td>'.$key['producto_descripcion'].'</td>
                            <td>$'.number_format($key['producto_valor_compra'], 0, ',', '.').'</td>
                            <td>$'.number_format($key['producto_valor_venta'], 0, ',', '.').'</td>
                            <td>'.$key['producto_stock_actual'].'</td>
                            <td>'.$key['producto_stock_minimo'].'</td>
                            <td>'.$key['tipo_producto_nombre'].'</td>
                        </tr>
                    ';
                    $i++;
                }
            ?>                              
        </tbody>
    </table>    
</div>

<script>
    // Definicion de la Tabla
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        keys: true,
        buttons: ['copy', 'excel', 'pdf']
    });
    // Definicion de Botones
    table.buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

    // Key Tables
    //$('#key-table').DataTable({keys: true});
    // Responsive Datatable
    //$('#responsive-datatable').DataTable();
    // Multi Selection Datatable
    //$('#selection-datatable').DataTable({ select: { style: 'multi'} });
</script>