<script>
    //////////////////////////////////////////////////////////////////////////////////////////////////
    $('#moduloPoducto_escanearProducto').click(function() 
    {
        console.log("Escanear Producto");
        var url = '/mca/index.php/Cproductos/vista_escanear_producto';
        $(document).ajaxStop($.unblockUI);
        $.blockUI({message: null,overlayCSS: { backgroundColor: '#007bff'} });
        $("#bodycentral").load(url, function(response,status, xhr) {});
    });

    $('#moduloPoducto_ListadoProducto').click(function() 
    {
        console.log("Listado de Productos");
        var url = '/mca/index.php/Cproductos/seleccion_listado_productos';
        $(document).ajaxStop($.unblockUI);
        $.blockUI({message: null,overlayCSS: { backgroundColor: '#007bff'} });
        $("#bodycentral").load(url, function(response,status, xhr) {});
    });

    $('#moduloPoducto_Inventario').click(function() 
    {
        console.log("Inventario");
        var url = '/mca/index.php/Cproductos/revisar_inventario';
        $(document).ajaxStop($.unblockUI);
        $.blockUI({message: null,overlayCSS: { backgroundColor: '#007bff'} });
        $("#bodycentral").load(url, function(response,status, xhr) {});
    });
    //////////////////////////////////////////////////////////////////////////////////////////////////
</script>