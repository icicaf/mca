<div class="card-box">
    <h4 class="header-title m-t-0"><?php echo $realizar_accion;?></h4>
    <p class="text-muted font-14 m-b-20">
        <?php echo $descripcion_accion;?>
    </p>
    <div class="form-group row">
        <div class="col-sm-2"></div>

        <div class="col-sm-8">
            <form id="formulario_agregar_usuario" name="formulario_agregar_usuario">
                <?php
                    $usuario_id = "";
                    $usuario_username = "";
                    $usuario_password = "";
                    $usuario_nombre = "";
                    $usuario_apellido = "";
                    $usuario_rut = "";
                    $usuario_correo = "";
                    $usuario_fecha_creacion = "";
                    $usuario_fecha_modificacion = "";
                    $tipo_usuario_id = "";
                    $tipo_usuario_nombre = "";

                    $visible = "";
                    $editable = "";
                    $requiere = "";

                    $btn_guarda = "";
                    $btn_actualiza = "";
                    $btn_elimina = "";
                    $btn_volver = "";
                    $btn_limpiar = "";

                    if($detalle_usuario!=null){
                        foreach ($detalle_usuario AS $key) {
                            $aux = explode(' ', $key["usuario_nombre"]);

                            $usuario_id = $key["usuario_id"];
                            $usuario_username = $key["usuario_username"];
                            $usuario_password = $key["usuario_password"];
                            $usuario_nombre = $aux[0];
                            $usuario_apellido = $aux[1];
                            $usuario_rut = $key["usuario_rut"];
                            $usuario_correo = $key["usuario_correo"];
                            $usuario_fecha_creacion = $key["usuario_fecha_creacion"];
                            $usuario_fecha_modificacion = $key["usuario_fecha_modificacion"];
                            $tipo_usuario_id = $key["tipo_usuario_id"];
                            $tipo_usuario_nombre = $key["tipo_usuario_nombre"];

                            $visible = "";
                            $editable = "disabled";
                            $requiere = "required";

                            $btn_guarda = "hidden";
                            $btn_actualiza = "";
                            $btn_elimina = "";
                            $btn_volver = "";
                            $btn_limpiar = "hidden";
                        }
                    } else {
                        $usuario_id = "";
                        $usuario_username = "";
                        $usuario_password = "";
                        $usuario_nombre = "";
                        $usuario_rut = "";
                        $usuario_correo = "";
                        $usuario_fecha_creacion = "";
                        $usuario_fecha_modificacion = "";
                        $tipo_usuario_id = "";
                        $tipo_usuario_nombre = "";

                        $visible = "hidden";
                        $editable = "";
                        $requiere = "";

                        $btn_guarda = "";
                        $btn_actualiza = "hidden";
                        $btn_elimina = "hidden";
                        $btn_volver = "hidden";
                        $btn_limpiar = "";
                    }
                    echo '
                    <div class="form-group row">
                        <label for="nombre" class="col-4 col-form-label">NOMBRE <span class="text-danger">*</span></label>
                        <div class="col-7">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="mdi mdi-account"></i></span>
                                <input style="font-weight: bold;" type="text" class="form-control" id="usuario_nombre" name="usuario_nombre" placeholder="..." value="'.$usuario_nombre.'" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" >
                        <label for="nombre" class="col-4 col-form-label">APELLIDOS <span class="text-danger">*</span></label>
                        <div class="col-7">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="mdi mdi-account"></i></span>
                                <input style="font-weight: bold;" type="text" class="form-control" id="usuario_apellidos" name="usuario_apellidos" placeholder="..." value="'.$usuario_apellido.'" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row" '.$visible.'>
                        <label for="apellido" class="col-4 col-form-label">USUARIO <span class="text-danger">*</span></label>
                        <div class="col-7">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="mdi mdi-account"></i></span>
                                <input style="font-weight: bold;" type="text" class="form-control" id="usuario_username" name="usuario_username" placeholder="..." value="'.$usuario_username.'" '.$editable.'>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row" '.$visible.'>
                        <label for="apellido" class="col-4 col-form-label">PASSWORD <span class="text-danger">*</span></label>
                        <div class="col-7">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="mdi mdi-key-variant"></i></span>
                                <input style="font-weight: bold;" type="text" class="form-control" id="usuario_password" name="usuario_password" placeholder="..." value="'.$usuario_password.'" '.$requiere.'>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="apellido" class="col-4 col-form-label">RUT (xxxxxxxx-x)<span class="text-danger">*</span></label>
                        <div class="col-7">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="mdi mdi-fingerprint"></i></span>
                                <input style="font-weight: bold;" type="text" class="form-control" id="usuario_rut" name="usuario_rut" placeholder="..." value="'.$usuario_rut.'" '.$editable.' '.$editable.' required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="apellido" class="col-4 col-form-label">E-MAIL <span class="text-danger">*</span></label>
                        <div class="col-7">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="mdi mdi-gmail"></i></span>
                                <input style="font-weight: bold;" type="text" class="form-control" id="usuario_correo" name="usuario_correo" placeholder="..." value="'.$usuario_correo.'" required>
                                <span id="valida_email"></span>
                            </div>
                        </div>
                    </div>
                 
                    <div class="form-group row">
                        <label class="col-4 col-form-label">Tipo Usuario</label>
                        <div class="col-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="mdi mdi-chevron-double-down"></i></span>
                                <select class="form-control input-sm text-center text-uppercase" id="select_tipo_usuaro" name="select_tipo_usuaro" required>
                                    <option value='.$tipo_usuario_id.'>'.$tipo_usuario_nombre.'</option>';
                                    foreach ($tipos_usuarios as $key2) {
                                        echo '<option value="'.$key2["tipo_usuario_id"].'" >'.$key2["tipo_usuario_nombre"].'</option>';
                                    }
                                    echo '
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <input type="number" class="form-control" id="usuario_id" name="usuario_id" value="'.$usuario_id.'" hidden>
                        
                    <div class="form-group row">
                        <div class="col-8 offset-4">
                            <button type="button" class="btn btn-success waves-effect waves-light" id="btn_guardar_usuario" name="btn_guardar_usuario" '.$btn_guarda.'><i class="mdi mdi-content-save"></i> Guardar</button>
                            <button type="button" class="btn btn-primary waves-effect waves-light" id="btn_actualizar_usuario" name="bbtn_actualizar_usuario" '.$btn_actualiza.'><i class="mdi mdi-content-save"></i> Actualizar</button>
                            <button type="button" class="btn btn-warning waves-effect waves-light" id="btn_eliminar_usuario" name="btn_eliminar_usuario" '.$btn_elimina.'><i class="mdi mdi-delete"></i> Eliminar</button>
                            <button type="reset"  class="btn btn-secondary waves-effect waves-light" '.$btn_limpiar.'><i class="mdi mdi-broom"></i> Limpiar</button>
                            <button type="button" class="btn btn-secondary waves-effect waves-light" id="btn_volver" name="btn_volver" '.$btn_volver.'><i class="ion-reply"></i> Volver</button>

                        </div>
                    </div>
                    ';
                ?>
            </form>
        </div>

        <div class="col-sm-2"></div>
    </div>
</div>
<br>
<br>

<script>
    //////////////////////////////////////////////////////////////////////////////////////////////////
    $(document).ready(function() 
    {
        document.getElementById('usuario_correo').addEventListener('input', function() {
            campo = event.target;
            valido = document.getElementById('valida_email');
            
            emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
            //Se muestra un texto a modo de ejemplo, luego va a ser un icono
            if (emailRegex.test(campo.value)) {
                valido.innerText = "Correo Válido";
            } else {
                valido.innerText = "Correo Incorrecto";
            }
        });
        var Fn = {
            // Valida el rut con su cadena completa "XXXXXXXX-X"
            validaRut : function (rutCompleto) {
                rutCompleto = rutCompleto.replace("‐","-");
                if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( rutCompleto ))
                return false;

                var tmp     = rutCompleto.split('-');
                var digv    = tmp[1]; 
                var rut     = tmp[0];
                if ( digv == 'K' ) digv = 'k' ;
                return (Fn.dv(rut) == digv );
            },
            dv : function(T){
                var M=0,S=1;
                for(;T;T=Math.floor(T/10))
                    S=(S+T%10*(9-M++%6))%11;
                return S?S-1:'k';
            }
        }

        $.valida_formulario_ingreso_usuario = function(){
            var errores = 0;
            $("#formulario_agregar_usuario").find("select").each(function () {
                var nombre_elemento = $(this).attr("name");
                if($(this).prop('required')) {
                    if($(this).val().length < 1) {
                        console.log('ERRORES : '+nombre_elemento);
                        errores++;
                    }
                }
            });
            $("#formulario_agregar_usuario").find("input").each(function () {
                var nombre_elemento = $(this).attr("name");
                if($(this).prop('required')) {
                    if($(this).val().length < 1) {
                        console.log('ERRORES : '+nombre_elemento);
                        errores++;
                    }
                }
            });
            return errores;
        }

        $("#btn_guardar_usuario").click(function(){
            if($.valida_formulario_ingreso_usuario() == 0){
                console.log($.valida_formulario_ingreso_usuario());
                $.ingresar_datos_usuario(1);
            }else{
                $.Notification.notify('error','bottom right','COMPLETE FORMULARIO', 'Debe ingresar todos los datos para el correcto ingreso del producto.');
                console.log($.valida_formulario_ingreso_usuario());
            }
        });
        $("#btn_actualizar_usuario").click(function(){
            if($.valida_formulario_ingreso_usuario() == 0){
                console.log($.valida_formulario_ingreso_usuario());
                $.ingresar_datos_usuario(2);
            }else{
                $.Notification.notify('error','bottom right','COMPLETE FORMULARIO', 'Debe ingresar todos los datos para el correcto ingreso del producto.');
                console.log($.valida_formulario_ingreso_usuario());
            }
        });
        $.ingresar_datos_usuario = function($accion)
        {
            if (Fn.validaRut( $("#usuario_rut").val() )){
                console.log("El rut ingresado es válido :D");
                switch ($accion){
                    case 1:$.ingresa_nuevo_usuario();break;
                    case 2:$.actualizar_usuario();break;
                }
            } else {
                console.log("El Rut no es válido :'( ");
                $.Notification.notify('error','bottom right','RUT INVORRECTO', 'Verifique el Rut del usuario para continuar.');
            }
        }

        $.ingresa_nuevo_usuario = function(){
            var formAddProducto = $('#formulario_agregar_usuario');
            var serialized = formAddProducto.serialize().split('=');

            //Muestra el formulario que se esta enviando
            var arreglo = new Array();
            var datos_formulario = new FormData(document.forms.namedItem("formulario_agregar_usuario"));
            for (var pair of datos_formulario.entries()) {
                console.log('Elemento: '+pair[0]+ ', Valor: ' + pair[1]);
                arreglo.push(pair[0]+'='+pair[1]);
            }
            console.log(arreglo);

            var url = '/mca/index.php/Cusuarios/ingresar_nuevo_usuario';
            $.ajax({
                type: "POST",
                url: url,
                data: { user_nombre:arreglo[0].split('=')[1],
                        user_apellidos:arreglo[1].split('=')[1],
                        user_rut:arreglo[4].split('=')[1],
                        user_correo:arreglo[5].split('=')[1],
                        select_tipo_usuario:arreglo[6].split('=')[1],
                        id_user:arreglo[7].split('=')[1] },
                success: function(msg) {
                    console.log(msg);
                    if(msg==1){
                        console.log("Datos Ingresados");
                        $.volver();
                        $.Notification.notify('success','bottom right','DATOS INGRESADOS', 'Se han guardado los datos del usuario Correctamente.');
                    }else{
                        $.Notification.notify('error','bottom right','ERROR DE INGRESO', 'No se ha podido ingresaractualizar el usuario Correctamente.');
                    }
                },
                error: function() {
                    console.log("error");
                }
            });
        };
        $.actualizar_usuario = function(){
            var formAddProducto = $('#formulario_agregar_usuario');
            var serialized = formAddProducto.serialize().split('=');

            //Muestra el formulario que se esta enviando
            var arreglo = new Array();
            var datos_formulario = new FormData(document.forms.namedItem("formulario_agregar_usuario"));
            for (var pair of datos_formulario.entries()) {
                console.log('Elemento: '+pair[0]+ ', Valor: ' + pair[1]);
                arreglo.push(pair[0]+'='+pair[1]);
            }
            console.log(arreglo);

            var url = '/mca/index.php/Cusuarios/actualizar_usuario';
            $.ajax({
                type: "POST",
                url: url,
                data: { user_nombre:arreglo[0].split('=')[1],
                        user_apellidos:arreglo[1].split('=')[1],
                        user_password:arreglo[2].split('=')[1],
                        user_correo:arreglo[3].split('=')[1],
                        select_tipo_usuario:arreglo[4].split('=')[1],
                        id_user:arreglo[5].split('=')[1] },
                success: function(msg) {
                    console.log(msg);
                    if(msg==1){
                        console.log("Datos Ingresados");
                        $.volver();
                        $.Notification.notify('success','bottom right','DATOS INGRESADOS', 'Se han actualizado los datos del usuario Correctamente.');
                    }else{
                        $.Notification.notify('error','bottom right','ERROR DE INGRESO', 'No se ha podido actualizar el  usuario Correctamente.');
                    }
                },
                error: function() {
                    console.log("error");
                }
            });
        }

        $("#btn_eliminar_usuario").click(function(){
            var id_usuario = $("#usuario_id").val();
            var url = '/mca/index.php/Cusuarios/eliminar_usuario';
            $.ajax({
                type: "POST",
                url: url,
                data: { id_usuario:id_usuario},
                success: function(msg) {
                    console.log(msg);
                    if(msg==1){
                        console.log("Usuario");
                        $.volver();
                        $.Notification.notify('success','bottom right','USUARIO ELIMINADO', 'Se ha eliminado correctamente el usuario del sistema.');
                    }else{
                        $.Notification.notify('error','bottom right','ERROR ELIMINANDO', 'No se ha podido eliminar el nuevo usuario Correctamente.');
                    }
                },
                error: function() {
                    console.log("error");
                }
            });
        });

        $("#btn_volver").click(function(){
            $.volver();
        });
        $.volver = function(){
            var url = '/mca/index.php/Cusuarios/gestion_usuarios_registrados';
            $(document).ajaxStop($.unblockUI);
            $.blockUI({message: null,overlayCSS: { backgroundColor: '#007bff'} });
            $("#bodycentral").load(url, function(response,status, xhr) {});
        };
    //////////////////////////////////////////////////////////////////////////////////////////////////
    });
</script>