<div class="row">
    <div class="col-12">
        <div class="card-box table-responsive">
            <h4 class="m-t-0 header-title">LISTADO DE PRODUCTOS EN EXISTENCIA</h4>
            <p class="text-muted font-14 m-b-30">
                Listado de usuarios activos dentro del sistema.
            </p>

            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" role="grid" aria-describedby="datatable-buttons_info" style="width: 100%;">
                <thead>
                    <tr>
                        <th class="info" style="text-align:center;">#</th>
                        <th class="info" style="text-align:center;">Nombre</th>
                        <th class="info" style="text-align:center;">Usuario</th>
                        <th class="info" style="text-align:center;">Password</th>
                        <th class="info" style="text-align:center;">Rut</th>
                        <th class="info" style="text-align:center;">E-Mail</th>
                        <th class="info" style="text-align:center;">Fecha Creacion</th>
                        <th class="info" style="text-align:center;">Ultima Modificacion</th>
                        <th class="info" style="text-align:center;">Tipo Usuario</th>
                        <th class="info" style="text-align:center;">Editar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i=1;
                        foreach($usuarios as $key){
                            echo '
                                <tr id="fila_'.$i.'" value="'.$i.'" style="text-align:center;">
                                    <td>'.$i.'</td>
                                    <td>'.$key['usuario_nombre'].'</td>
                                    <td>'.$key['usuario_username'].'</td>
                                    <td>'.$key['usuario_password'].'</td>
                                    <td>'.$key['usuario_rut'].'</td>
                                    <td>'.$key['usuario_correo'].'</td>
                                    <td>'.$key['usuario_fecha_creacion'].'</td>
                                    <td>'.$key['usuario_fecha_modificacion'].'</td>
                                    <td>'.$key['tipo_usuario_nombre'].'</td>
                                    <th style="text-align:center;">
                                        <acronym title="Editar Usuario">
                                            <button type="button" 
                                                    value="'.$key["usuario_id"].'" 
                                                    name="btn_editar_usuario" 
                                                    id="'.$i.'"
                                                    class="btn btn-warning waves-effect waves-light">
                                                <i class="fa fa-pencil"></i>                                                                
                                            </button>
                                        </acronym>
                                    </th>
                                </tr>
                            ';
                            $i++;
                        }
                    ?>                              
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    // Definicion de la Tabla
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        keys: true,
        buttons: ['copy', 'excel', 'pdf'],
        lengthMenu: [[25, 50, 75, -1], [25, 75, 125, "Todos"]],
        fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ){                
            //$('td:eq(8)', nRow).html( "<div align='center'><button class='btn btn-danger btn-circle disabled'></button></div>");
        },
    });
    // Definicion de Botones
    table.buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
    // Key Tables
    //$('#key-table').DataTable({keys: true});
    // Responsive Datatable
    //$('#responsive-datatable').DataTable();
    // Multi Selection Datatable
    //$('#selection-datatable').DataTable({ select: { style: 'multi'} });

    //////////////////////////////////////////////////////////////////////////////////////////////////
    $('button[name="btn_editar_usuario"]').click(function(){
        var id_usuario = $(this).attr('value');
        console.log("BTN id_usuario:"+id_usuario);

        $.Notification.notify('success','bottom right','DETALLES DEL USUARIO', '');
        var url = '/mca/index.php/Cusuarios/get_detalle_usuario';
        $.ajax({
            type: "POST",
            url: url,
            data: {id_usuario:id_usuario},
            success: function(msg) {
                $("#bodycentral").html(msg);
            },
            error: function() {
                console.log("error");
            }
        });
    });
    //////////////////////////////////////////////////////////////////////////////////////////////////
</script>