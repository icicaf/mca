<?php
    
   // print_r($data_venta[0]["venta_fecha"]);

    $text_1 = '7px;'; // descripion producto

    $head ='<table style="font-size:'.$text_1.'" align="center">
                <tr>
                    <td><b>Comprobante de Venta</b></td>
                </tr>
            </table>
            <table style="font-size:'.$text_1.'" align="center">
                 <tr>
                    <td style="height:0px;">Nombre de la empresa</td>
                 </tr>
                 <tr>
                    <td style="height:0px;">Rut de la empresa</td>
                 </tr>
             </table>';

    echo $head;

    echo '<br><br>';

    $detalle_venta ='<table style="font-size:'.$text_1.'" align="center">
                <tr>
                    <td width="10"></td>
                    <td width="100" style="text-align:left;"><b>Fecha</b></td>
                    <td style="text-align:right;"><b>Hora</b></td>
                    <td width="10"></td>
                </tr>
                <tr>
                    <td width="10"></td>
                    <td width="100" style="text-align:left;">'.$data_venta[0]["venta_fecha"].'</td>
                    <td style="text-align:right;">'.$data_venta[0]["venta_hora"].'</td>
                    <td width="10"></td>
                </tr>
            </table>';

    echo $detalle_venta;
            
    echo '<br><br>';

    $inicio_body ='<table style="font-size:'.$text_1.'" align="left">
                <tr>
                    <td width="10"></td>
                    <td style="height:10px;"><b>Detalle venta</b></td>
                </tr>
            </table>';

    echo $inicio_body;
        
    $html_productos ='<table style="font-size:'.$text_1.'" border="0">';

    //DATA
    //print_r($data_venta[0]);

    //print_r($data_venta[0]["venta_detalle"]);

    $xml = simplexml_load_string($data_venta[0]["venta_detalle"]);

    //print_r($xml);
    
    foreach ($xml->item as $key) 
    {
        $html_productos.='<tr>
                    <td width="10"></td>
                    <td width="100" style="text-align: center;">'.$key->cantidad_producto.' X '.$key->precio_producto.'</td>
                    <td></td>
                    <td width="10"></td>
                </tr>';

        $html_productos.='<tr>
                    <td width="10"></td>
                    <td width="100">'.$key->nombre_producto.'</td>
                    <td style="text-align: right;">$ '.$key->cantidad_producto * $key->precio_producto.'</td>
                    <td width="10"></td>
                </tr>
                <br>';
    }

    echo $html_productos;


    // $html_subtotal= '<tr>
    //             <td width="10"></td>
    //             <td>SUBTOTAL</td>
    //         </tr>';

    // echo $html_subtotal;

    $text_2 = '8px';
    $html_total= '<table style="font-size:'.$text_2.'" border="0">
                <tr>
                <td width="10"></td>
                <td><b>TOTAL</b></td>
                <td  width="100" style="text-align: right;"><b>$ '.$xml->total->total_venta.'</b></td>
                <td width="10"></td>
            </tr>
        </table>';

    echo $html_total;

?>