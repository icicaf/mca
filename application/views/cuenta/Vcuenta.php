<div class="row">
    <div class="col-xl-3 col-lg-4">
        <div class="text-center card-box">
            <div class="member-card">
                <div class="thumb-xl member-thumb m-b-10 center-block">
                    <img src="../assets/images/users/avatar-1.jpg" class="rounded-circle img-thumbnail" alt="profile-image">
                </div>
                <div class="">
                    <h5 class="m-b-5">nombre usuario</h5>
                    <p class="text-muted">correo</p>
                </div>
                <button type="button" class="btn btn-success btn-sm w-sm waves-effect m-t-10 waves-light">Follow</button>
                <button type="button" class="btn btn-danger btn-sm w-sm waves-effect m-t-10 waves-light">Message</button>
                <div class="text-left m-t-40">
                    <p class="text-muted font-13"><strong>Nombre :</strong> <span class="m-l-15">Mark A. McKnight</span></p>
                    <p class="text-muted font-13"><strong>Teléfono :</strong><span class="m-l-15">(123) 123 1234</span></p>
                    <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">coderthemes@gmail.com</span></p>
                </div>
            </div>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
    <div class="col-lg-8 col-xl-9">
        <div class="">
            <div class="card-box">
                <ul class="nav nav-tabs tabs-bordered">
                    <li class="nav-item">
                        <a href="#micuenta" data-toggle="tab" aria-expanded="true" class="nav-link">
                            MI CUENTA
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="micuenta">
                        <form role="form">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" value="" id="nombre" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="correo">Correo</label>
                                <input type="email" value="" id="correo" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="usuario">Usuario</label>
                                <input type="text" value="" id="usuario" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="clave">Contraseña</label>
                                <input type="password" placeholder="6 - 15 caracteres" id="clave" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="reclave">Reingrese-Contraseña</label>
                                <input type="password" placeholder="6 - 15 caracteres" id="reclave" class="form-control">
                            </div>
                            <div class="form-group">
                            <button class="btn btn-primary waves-effect waves-light w-md" type="submit">Guardar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div>
<!-- end row -->