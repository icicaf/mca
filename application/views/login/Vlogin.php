<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>MCA - Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/images/favicon.ico">

        <!-- App css -->
        <link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="<?php echo base_url(); ?>/assets/js/modernizr.min.js"></script>

    </head>
    <body>

        <div class="wrapper-page">
        <div class="container-fluid">
            <div class="text-center">
                <a href="#" class="logo-lg"><i class="mdi mdi-radar"></i> <span>MCA</span> </a>
            </div>
            <form class="form-horizontal m-t-20" action="<?php echo base_url(); ?>Clogin/validar_usuario" method="post">
            <form id="mca_login" name="mca_login" class="form-horizontal m-t-20" action="index.html">
                <div class="form-group row">
                    <div class="col-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="mdi mdi-account"></i></span>
                            <input class="form-control" name="usuario" type="text" required="" placeholder="Usuario">
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="mdi mdi-radar"></i></span>
                            <input class="form-control" name="clave" type="password" required="" placeholder="Contraseña">
                        </div>
                    </div>
                </div>
                <?php
                    if($this->session->flashdata('login'))
                    {
                        echo '<div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    Usuario o Contraseña incorrectos
                            </div>';
                    }
                    else 
                    {
                        //no hace nada
                    }
                ?>
                <div class="form-group row">
                    <div class="col-12">
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox-signup" type="checkbox">
                            <label for="checkbox-signup">
                                Recordar cuenta
                            </label>
                        </div>

                    </div>
                </div>

                <div class="form-group text-right m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-custom w-md waves-effect waves-light" type="submit">Ingresar
                        </button>
                    </div>
                </div>

                <div class="form-group row m-t-30">
                    <div class="col-sm-7">
                        <a href="#" class="text-muted"><i class="fa fa-lock m-r-5"></i> Olvido su cuenta ?</a>
                    </div>
                    <div class="col-sm-5 text-right">
                        <a href="javascript:location.href = 'http://closekiosk';"  class="text-muted">Salir</a>
                    </div>
                </div>
            </form>
        </div>
        </div>

        <!-- jQuery  -->
        <script src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/popper.min.js"></script><!-- Popper for Bootstrap --><!-- Tether for Bootstrap -->
        <script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/waves.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="<?php echo base_url(); ?>/assets/js/jquery.core.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/jquery.app.js"></script>

	</body>
</html>