<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//LIBRERIA PARA GENERAR UNA BOLETA

class Boleta
{
    public function imprimir_comprobante_venta($data_total_venta)
    {
        $salto_linea = 20;
        $linea = 0;

        $handle = printer_open('post_impresora');
        printer_start_doc($handle, "boleta");
        printer_start_page($handle);

        $font = printer_create_font("Arial", 17, 8, PRINTER_FW_NORMAL, false, false, false, 0);
        printer_select_font($handle, $font);
        printer_draw_text($handle, "COMPROBANTE DE VENTA", 70, $linea);
        $linea = $linea + $salto_linea;
        printer_draw_text($handle, "RUT: 16.675.738-K", 100, $linea);
        $linea = $linea + $salto_linea;
        printer_draw_text($handle, "NOMBRE DEL GIRO O NOMBRE DEL NEGOCIO", 0, $linea);
        $linea = $linea + $salto_linea;
        printer_draw_text($handle, utf8_encode("DIRECCION DEL LOCAL"), 50, $linea);
        $linea = $linea + $salto_linea;
        $linea = $linea + $salto_linea;
        printer_draw_text($handle, 'FECHA', 0, $linea);
        printer_draw_text($handle, 'HORA', 180, $linea);
        printer_draw_text($handle, 'TERMINAL', 320, $linea);
        $linea = $linea + $salto_linea;
        printer_draw_text($handle, date('d-m-Y'), 0, $linea);
        printer_draw_text($handle, date('H:i'), 180, $linea);
        printer_draw_text($handle, 'CAJA 1', 320, $linea);

        $linea = $linea + $salto_linea;
        printer_delete_font($font);

        $fontprd = printer_create_font("Arial", 17, 8, PRINTER_FW_NORMAL, false, false, false, 0);
        printer_select_font($handle, $fontprd);
        printer_draw_text($handle, "RUT: 16.675.738-K", 60, $linea);
        $linea = $linea + $salto_linea;

        $linea = $linea + $salto_linea;
        printer_draw_text($handle, "TOTAL VENTA $ ".$data_total_venta, 60, $linea);

        $linea =( $linea + $salto_linea)*2;
        printer_draw_text($handle, "GRACIAS POR SU COMPRA", 60, $linea);
        printer_delete_font($font);

        // echo $linea;
        // $brush = printer_create_brush(PRINTER_BRUSH_SOLID, "2222FF");
        // printer_select_brush($handle, $brush);
        // printer_draw_rectangle($handle, $linea, 10, 0, $linea);
        // printer_delete_brush($brush);
        // printer_delete_pen($pen);

        printer_end_page($handle);
        printer_end_doc($handle);
        printer_close($handle);
    }
}