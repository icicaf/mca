<?php
/**
*   Clase Minicio Modelo
*
*   @author     Christian Castillo Pineda
*   @since      Version 1.0
*/
class Minicio extends CI_Model
{
    private $db_mca;
    function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('default', true);
    }
    //
    //---------------------------------------------------------------------------------------------------------
    //
    public function inicio_fin_semana()
    {
	    $hoy = date("Y-m-d");
	    $diaInicio="Monday";
	    $diaFin="Sunday";

	    $strFecha = strtotime($hoy);

	    $fechaInicio = date('Y-m-d',strtotime('last '.$diaInicio,$strFecha));
	    $fechaFin = date('Y-m-d',strtotime('next '.$diaFin,$strFecha));

	    if(date("l",$strFecha)==$diaInicio){
	        $fechaInicio= date("Y-m-d",$strFecha);
	    }
	    if(date("l",$strFecha)==$diaFin){
	        $fechaFin= date("Y-m-d",$strFecha);
	    }
	    return Array("fechaInicio"=>$fechaInicio,"fechaFin"=>$fechaFin);
	}

    public function get_venta_diaria()
    {
    	$hoy = date("Y-m-d");
    	$fecha_hoy = date("d-m-Y");
    	$consulta = 'SELECT SUM(venta_valor) AS venta_diaria, 
    						"'.$fecha_hoy.'" AS venta_fecha 
					FROM ventas
					WHERE venta_fecha ="'.$hoy.'"';
        //DEBUG
        //echo $consulta; 
        $query = $this->db->query($consulta);
        $data = $query->result_array();
        //print_r($data);
        return $data;
    }

    public function get_venta_semanal()
    {
    	$lunes;
    	$fechas = $this->inicio_fin_semana();
		$lunes = $fechas["fechaInicio"];
    	$hoy = date("Y-m-d");

    	$consulta = 'SELECT SUM(venta_valor) AS venta_semanal
					 FROM ventas
					 WHERE venta_fecha BETWEEN "'.$lunes.'" AND "'.$hoy.'"';
        //DEBUG
        //echo $consulta; 
        $query = $this->db->query($consulta);
        $data = $query->result_array();
        //print_r($data);
        return $data;
    }

    public function get_venta_mensual()
    {
    	$inicio_mes = date("Y-m-01");
    	$hoy = date("Y-m-d");

    	$consulta = 'SELECT SUM(venta_valor) AS venta_mensual
					 FROM ventas
					 WHERE venta_fecha BETWEEN "'.$inicio_mes.'" AND "'.$hoy.'"';
        //DEBUG
        //echo $consulta; 
        $query = $this->db->query($consulta);
        $data = $query->result_array();
        //print_r($data);
        return $data;
    }
}