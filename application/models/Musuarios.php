<?php
/**
*   Clase Musuarios Modelo
*
*   @author     Cristian Aguayo Forteza
*   @since      Version 1.0
*/
class Musuarios extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('default',TRUE);
    }

    public function get_usuarios()
    {
        $consulta = 'SELECT U.usuario_id,
                            U.usuario_username,
                            U.usuario_password,
                            U.usuario_nombre,
                            U.usuario_rut,
                            U.usuario_correo,
                            U.usuario_fecha_creacion,
                            U.usuario_fecha_modificacion,
                            U.tipo_usuario_id,
                            TU.tipo_usuario_nombre
                    FROM usuarios AS U
                    INNER JOIN tipo_usuarios AS TU ON TU.tipo_usuario_id = U.tipo_usuario_id
                    WHERE U.usuario_habilitado=1
                    ORDER BY U.usuario_id ASC';
        //DEBUG
        //echo $consulta;
        return $this->db->query($consulta)->result_array();
    }

    public function get_usuario_login($usuario,$clave)
    {
        $consulta = 'SELECT 
                        usuario_id, -- int(11) AI PK 
                        usuario_username, -- varchar(45) 
                        usuario_password,-- varchar(45) 
                        usuario_nombre,-- varchar(200) 
                        usuario_rut,-- varchar(45) 
                        usuario_correo,-- varchar(45) 
                        tipo_usuario_id -- int(2)
                    FROM
                        usuarios
                    WHERE
                        BINARY usuario_username = "'.$usuario.'" 
                        AND BINARY usuario_password = "'.$clave.'"';

        return $this->db->query($consulta)->result_array();
    }

    public function get_tipos_usuarios()
    {
        $consulta = 'SELECT * FROM tipo_usuarios WHERE tipo_usuario_id NOT IN (1,2)';
        //DEBUG
        //echo $consulta;
        return $this->db->query($consulta)->result_array();
    }

    //Ingresa usuario
    public function ingresar_datos_usuario($data_usuario)
    {
        $this->db->trans_begin();   
        $this->db->where('usuario_id',$data_usuario['usuario_id']);
        $aux = $this->db->get('usuarios');
        if($aux->num_rows()>0){
            //Actualiza
            $this->db->where('usuario_id',$data_usuario['usuario_id']);
            $this->db->update('usuarios', $data_usuario);
        }else{
            //Inserta
            $this->db->insert('usuarios', $data_usuario);
        }
        
        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_complete();
            return 0;
        }
        else
        {
            $this->db->trans_complete();
            return 1;
        }
    }

    public function get_usuarios_registrados()
    {
        $consulta = 'SELECT * FROM usuarios WHERE usuario_habilitado=1';
        //DEBUG
        //echo $consulta;
        return $this->db->query($consulta)->result_array();
    }

    public function get_detalle_usuario($id_usuario)
    {
        $consulta = 'SELECT U.usuario_id,
                            U.usuario_username,
                            U.usuario_password,
                            U.usuario_nombre,
                            U.usuario_rut,
                            U.usuario_correo,
                            U.usuario_fecha_creacion,
                            U.usuario_fecha_modificacion,
                            U.tipo_usuario_id,
                            TU.tipo_usuario_nombre
                    FROM usuarios AS U
                    INNER JOIN tipo_usuarios AS TU ON TU.tipo_usuario_id = U.tipo_usuario_id
                    WHERE U.usuario_id ='.$id_usuario;
        //DEBUG
        //echo $consulta;
        return $this->db->query($consulta)->result_array();
    }

    public function eliminar_usuario($data_usuario)
    {
        $this->db->trans_begin();   
        $this->db->where('usuario_id',$data_usuario['usuario_id']);
        $this->db->update('usuarios', $data_usuario);
        
        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_complete();
            return 0;
        }
        else
        {
            $this->db->trans_complete();
            return 1;
        }
    }
}