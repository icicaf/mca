<?php
/**
*   Clase Mproductos Modelo
*
*   @author     Christian Castillo Pineda
*   @since      Version 1.0
*/
class Mproductos extends CI_Model
{
    private $db_mca;
    function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('default', true);
    }
    //
    //---------------------------------------------------------------------------------------------------------
    //
    //Verifica si existe el producto para actualizar la informacion o insertar uno nuevo
    public function get_existencia_producto($product_code)
    {
        $consulta = 'SELECT COUNT(*) AS cantida_productos, producto_codigo, producto_habilitado FROM productos WHERE producto_codigo="'.$product_code.'"';
        //DEBUG
        //echo $consulta; 
        $query = $this->db->query($consulta);
        $data = $query->result_array();
        //print_r($data);
        return $data;
    }

    public function get_id_producto($product_code)
    {
        $consulta = 'SELECT producto_id
                    FROM productos
                    WHERE producto_codigo="'.$product_code.'"';
        //DEBUG
        //echo $consulta; 
        $query = $this->db->query($consulta);
        $data = $query->result_array();
        //print_r($data);
        return $data;
    }
    //Retorna el detalle del producto
    public function get_datos_producto($product_code)
    {
        $consulta = 'SELECT P.producto_id,
                            P.producto_codigo,
                            P.producto_nombre,
                            P.producto_descripcion,
                            P.producto_valor_compra,
                            P.producto_valor_venta,
                            P.producto_stock_actual,
                            P.producto_stock_minimo,
                            P.producto_fecha_ingreso,
                            T.tipo_producto_nombre,
                            P.usuario_id,
                            P.tipo_producto_id
                    FROM productos AS P
                    INNER JOIN tipo_productos AS T ON T.tipo_producto_id = P.tipo_producto_id
                    WHERE producto_codigo="'.$product_code.'"';
        //DEBUG
        //echo $consulta; 
        $query = $this->db->query($consulta);
        $data = $query->result_array();
        //print_r($data);
        return $data;
    }

    //Retorna todos los productos
    public function get_productos()
    {
        $consulta = 'SELECT * FROM productos WHERE producto_habilitado=1';
        //DEBUG
        //echo $consulta; 
        $query = $this->db->query($consulta);
        $data = $query->result_array();
        return $data;
    }

    //Ingresa producto nuevo o lo actualiza si existe
    public function ingresar_producto($data_producto,$data_producto_entrada)
    {
        //print_r($data_producto_entrada);   

        $this->db->trans_begin();   
        $this->db->where('producto_id',$data_producto['producto_id']);
        $this->db->where('producto_codigo',$data_producto['producto_codigo']);
        $aux = $this->db->get('productos');
        if($aux->num_rows()>0){
            //Actualiza
            $this->db->where('producto_id',$data_producto['producto_id']);
            $this->db->where('producto_codigo',$data_producto['producto_codigo']);
            $this->db->update('productos', $data_producto);
        }else{
            //Inserta
            $this->db->insert('productos', $data_producto);
        }
        //Inserta Registro de Entrada de Producto
        $this->db->insert('producto_entradas', $data_producto_entrada);
        
        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_complete();
            return 0;
        }
        else
        {
            $this->db->trans_complete();
            return 1;
        }
    }

    //Elimina el producto solicitado (Le elimina visualmente, lo deshabilita en el sistema)
    public function deshabilitar_producto($id_producto)
    {
        $data = array('producto_habilitado' => 0);
        
        $this->db->trans_begin();
        $this->db->where('producto_id',$id_producto);
        $this->db->update('productos',$data);

        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_complete();
            return 0;
        }
        else
        {
            $this->db->trans_complete();
            return 1;
        }
    }
    //
    //---------------------------------------------------------------------------------------------------------
    //
    public function get_tipos_productos()
    {
        $consulta = 'SELECT * FROM tipo_productos';
        //DEBUG
        //echo $consulta; 
        $query = $this->db->query($consulta);
        $data = $query->result_array();
        return $data;
    }

    public function get_listado_productos_segun_tipo($tipo_producto_id)
    {
        $consulta = 'SELECT P.producto_id,
                            P.producto_codigo,
                            P.producto_nombre,
                            P.producto_descripcion,
                            P.producto_valor_compra,
                            P.producto_valor_venta,
                            P.producto_stock_actual,
                            P.producto_stock_minimo,
                            P.producto_fecha_ingreso,
                            T.tipo_producto_nombre,
                            P.usuario_id
                    FROM productos AS P
                    INNER JOIN tipo_productos AS T ON T.tipo_producto_id = P.tipo_producto_id
                    WHERE P.tipo_producto_id ='.$tipo_producto_id;
        //DEBUG
        //echo $consulta; 
        $query = $this->db->query($consulta);
        $data = $query->result_array();
        return $data;
    }

    public function get_cantidad_productos_tipo()
    {
        $consulta = 'SELECT T1.tipo_producto_id,
                            T1.tipo_producto_nombre,
                            COUNT(P.producto_id) AS cant_productos
                    FROM productos AS P
                    INNER JOIN tipo_productos AS T1 ON T1.tipo_producto_id = P.tipo_producto_id
                    GROUP BY P.tipo_producto_id';
        //DEBUG
        //echo $consulta; 
        $query = $this->db->query($consulta);
        $data = $query->result_array();
        return $data;
    }

    public function get_detalle_tipo_productos($producto_id)
    {
        $consulta = 'SELECT T1.tipo_producto_id,
                            T1.tipo_producto_nombre,
                            COUNT(P.producto_id) AS cant_productos,
                            SUM(IF(producto_stock_actual < producto_stock_minimo,1,0)) AS productos_bajo_stock_minimo,
                            SUM(IF(producto_stock_actual = 0, 1, 0)) AS productos_sin_stock,
                            SUM(IF(producto_habilitado = 0, 1, 0)) AS productos_deshabilitados,
                            SUM(IF(producto_valor_venta = 0, 1, 0)) AS productos_sin_precio
                    FROM productos AS P
                            INNER JOIN
                        tipo_productos AS T1 ON T1.tipo_producto_id = P.tipo_producto_id
                        WHERE T1.tipo_producto_id = '.$producto_id.'
                    GROUP BY P.tipo_producto_id';       
        //DEBUG
        //echo $consulta; 
        $query = $this->db->query($consulta);
        $data = $query->result_array();
        return $data;
    }
}