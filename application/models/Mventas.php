<?php

/**
*   Clase Mventas Modelo
*
*   @author     Nicolas Miranda Contreras
*   @since      Version 1.0
*/

class Mventas extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', true);
	}

	//Traer informacion actual producto

	public function getDatosProducto($cb)
	{
       $SQL = 'SELECT 
					*
				FROM
					ch_mca.productos
				WHERE
					producto_codigo = "'.$cb.'"';

       $query = $this->db->query($SQL);
	   $data = $query->result_array();
	   return $data;
   }

    public function transact_venta($boleta_xml,$data_productos,$data_medio_pago,$data_total_venta,$data_usuario)
    {
    	//INICIO LA TRANSANCCION
        $this->db->trans_begin();

        //DESCUENTO EL O LOS PRODUCTOS VENDIDOS
        // 0 : cantidad de productos
        // 1 : codigo de producto
        // 2 : nombre de producto 

    	$venta_valor = $data_total_venta;
    	$tipo_pago_id = $data_medio_pago;
    	$usuario_id = $data_usuario;
    	$estado_venta_id = 1;

    	$venta_cantidad_productos = 0;
    	$venta_detalle = '';

        foreach ($data_productos as $key) 
        {
			$venta_cantidad_productos = $venta_cantidad_productos + $key[0];
			$SQL_1 = "UPDATE productos AS A 
						SET 
						    A.producto_stock_actual = (SELECT 
						            T1.cantidad
						        FROM
						            (SELECT 
						                (producto_stock_actual - ".$key[0].") AS cantidad
						            FROM
						                productos AS A
						            WHERE
						                producto_codigo = '".$key[1]."') AS T1)
						WHERE
						    producto_codigo = '".$key[1]."'";

			$this->db->query($SQL_1);

			//INSERTA EL REGISTRO DE LA SALIDA DE LOS ARTICULOS
			$SQL_3 = "INSERT INTO `ch_mca`.`producto_salidas` (`producto_id`, `producto_salida_cantidad`, `producto_salida_valor_venta`, `producto_salida_fecha`, `producto_salida_hora`, `motivo_salida_id`, `usuario_id`, `venta_id`) VALUES ('".$key[1]."', '".$key[0]."', '999', '".date('Y-m-d')."', '".date('H:i:s')."', '1', '".$usuario_id."','0');";

			$this->db->query($SQL_3);
		}

		//INSERTA EL REGISTRO DE LA VENTA
		$SQL_2 = "INSERT INTO `ch_mca`.`ventas` (`venta_valor`, `venta_cantidad_productos`, `venta_detalle`, `venta_fecha`, `venta_hora`, `usuario_id`, `tipo_pago_id`, `estado_venta_id`) VALUES ('".$venta_valor."', '".$venta_cantidad_productos."', '".$boleta_xml."', '".date('Y-m-d')."', '".date('H:i:s')."', '".$usuario_id."', '".$tipo_pago_id."', '".$estado_venta_id."');";

		$this->db->query($SQL_2);

		$id= $this->db->insert_id();

		$SQL_4 = "UPDATE `ch_mca`.`producto_salidas` SET `venta_id`='".$id."' WHERE `venta_id`= 0";
		$this->db->query($SQL_4);

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return 0;
		}
		else
		{
			$this->db->trans_commit();
			return $id;
		}
    }

    public function get_venta($id_venta)
    {
    	$SQL = "SELECT * FROM ventas where venta_id ='".$id_venta."'";

    	$query = $this->db->query($SQL);
	   	$data = $query->result_array();
	   	return $data;
    }
}