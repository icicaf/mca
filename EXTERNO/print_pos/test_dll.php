<?php

/* Both these tests were takes from the php.net website and comments */

/* Basic test */
// $handle = printer_open('pos_impresora');
// printer_write($handle, "Text to print\next to print<br>ext to print");
// printer_close($handle);
// exit;
    
/* Test if it can print its version of WordArt */
$handle = printer_open('pos_impresora');
printer_start_doc($handle, "boleta");
printer_start_page($handle);

$font = printer_create_font("Arial", 17, 8, PRINTER_FW_NORMAL, false, false, false, 0);
printer_select_font($handle, $font);
printer_draw_text($handle, "COMPROBANTE DE VENTA", 60, 0);
printer_delete_font($font);



printer_end_page($handle);
printer_end_doc($handle);
printer_close($handle);
exit;

?>